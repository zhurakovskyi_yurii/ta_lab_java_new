package com.epam.homeworks.logical.task.d.game;

import java.util.List;

public class RoundHall {
    private List<Door> doors;

    public RoundHall(List<Door> doors) {
        this.doors = doors;
    }

    public List<Door> getDoors() {
        return doors;
    }

}

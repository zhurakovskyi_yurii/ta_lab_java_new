package com.epam.homeworks.logical.task.d.game;

public enum TypeOfHiddenItem {
    MONSTER, ARTIFACT;
}

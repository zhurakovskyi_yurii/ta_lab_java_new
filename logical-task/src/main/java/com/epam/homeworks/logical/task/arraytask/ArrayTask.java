package com.epam.homeworks.logical.task.arraytask;

public class ArrayTask {
    private int[] array1;
    private int[] array2;

    public ArrayTask(int[] array1, int[] array2) {
        this.array1 = array1;
        this.array2 = array2;
    }

    public int[] getArray1() {
        return array1;
    }

    public int[] getArray2() {
        return array2;
    }

    public int[] getDuplicates() {
        int[] duplicates = new int[maxLength()];
        int counter = 0;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[j]) {
                    duplicates[counter++] = array1[i];
                    break;
                }
            }
        }
        return duplicates;
    }

    public int[] getUnique() {
        int[] unique = new int[sumLength()];
        int counter = 0;
        int[] elementsWhichExistOnlyInFirstArray = getElementsWhichExistOnlyInFirstArray();
        for (int i = 0; i < elementsWhichExistOnlyInFirstArray.length; i++) {
            unique[counter++] = elementsWhichExistOnlyInFirstArray[i];
        }
        int[] elementsWhichExistOnlyInSecondArray = getElementsWhichExistOnlyInSecondArray();
        for (int i = 0; i < elementsWhichExistOnlyInSecondArray.length; i++) {
            unique[counter++] = elementsWhichExistOnlyInSecondArray[i];
        }
        return unique;
    }

    public int[] getElementsWhichExistOnlyInFirstArray() {
        int[] uniqueElementsFromFirstArray = new int[array1.length];
        int count = 0;
        boolean hasDuplicate = false;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[i]) {
                    hasDuplicate = true;
                    break;
                }
            }
            if (!hasDuplicate)
                uniqueElementsFromFirstArray[count++] = array1[i];
        }
        return uniqueElementsFromFirstArray;
    }

    public int[] getElementsWhichExistOnlyInSecondArray() {
        int[] uniqueElementsFromFirstArray = new int[array1.length];
        int count = 0;
        boolean hasDuplicate = false;
        for (int i = 0; i < array1.length; i++) {
            for (int j = 0; j < array2.length; j++) {
                if (array1[i] == array2[i]) {
                    hasDuplicate = true;
                    break;
                }
            }
            if (!hasDuplicate)
                uniqueElementsFromFirstArray[count++] = array1[i];
        }
        return uniqueElementsFromFirstArray;
    }

    public int sumLength() {
        return array1.length + array2.length;
    }

    public int maxLength() {
        return array1.length > array2.length ? array1.length : array2.length;
    }
}

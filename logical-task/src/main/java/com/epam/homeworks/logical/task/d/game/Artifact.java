package com.epam.homeworks.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Artifact extends HiddenItem {
    private int power;
    private static final Logger logger = LogManager.getLogger(Artifact.class);

    public Artifact() {
        this.power = 10 + (int) (Math.random() * 80);
    }

    public void show(Hero hero) {
        logger.info("Here is an artifact. You got +" + this.power + "power! ");
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Artifact";
    }
}

package com.epam.homeworks.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Game {
    private RoundHall roundHall;
    private Hero hero;
    private Random random;
    private List<Door> doors;
    private static final Logger logger = LogManager.getLogger(Game.class);

    public Game() {
        random = new Random();
        doors = getRandomDoors();
        roundHall = new RoundHall(doors);
        hero = new Hero("Bob");
    }

    private List<Door> getRandomDoors() {
        List<Door> doors = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            if (random.nextBoolean())
                doors.add(new Door(new Monster()));
            else
                doors.add(new Door(new Artifact()));
        }
        return doors;
    }

    public RoundHall getRoundHall() {
        return roundHall;
    }

    public void play() {
        showHeroDetail();
        showRoundHall();
        logger.info("\n\nCount of deadly doors: " + calculateDeadlyDoor(roundHall.getDoors().size() - 1) + "\n");
        openDoor();
    }

    public void openDoor() {
        try (Scanner scanner = new Scanner(System.in)) {
            logger.info("\nWhich door you want to open(Enter a number 1-10):\n");
            int number = scanner.nextInt();
            switch (number) {
                case 1:
                	openIfItIsNotOpened(1);
                    break;
                case 2:
                    openIfItIsNotOpened(2);
                    break;
                case 3:
                    openIfItIsNotOpened(3);
                    break;
                case 4:
                    openIfItIsNotOpened(4);
                    break;
                case 5:
                    openIfItIsNotOpened(5);
                    break;
                case 6:
                    openIfItIsNotOpened(6);
                    break;
                case 7:
                    openIfItIsNotOpened(7);
                    break;
                case 8:
                    openIfItIsNotOpened(8);
                    break;
                case 9:
                    openIfItIsNotOpened(9);
                    break;
                case 10:
                    openIfItIsNotOpened(10);
                    break;
                default:
                    break;
            }
        }
    }

	private void openIfItIsNotOpened(int number) {
    	if (!doors.get(number).isOpened()) {
            doors.get(number).open(hero);
           if (checkTypeOfHiddenItem(number))
               hero.setPower(hero.getPower() + (10 + (int)(Math.random() * 80)));
        }
    	else logger.info("The door is already opened!");
	}

	private boolean checkTypeOfHiddenItem(int number){
        return doors.get(number).getHiddenItem().typeOfHiddenItem == TypeOfHiddenItem.ARTIFACT;
    }

	private void showHeroDetail() {
        logger.info("Hero's details:");
        logger.info("Name of hero: " + hero.getName());
        logger.info("Power of hero: " + hero.getPower() + "\n");
    }

    private void showRoundHall() {
        logger.info("the hero came into a round hall....\n");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < 10; i++) {
            logger.info("***********  ");
        }
        logger.info("\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 10; j++) {
                logger.info("*         *  ");
            }
            logger.info("\n");
        }
        for (int i = 0; i < 10; i++) {
            if (i < 9) {
                logger.info("*  DOOR " + (i + 1) + " *  ");
            } else
                logger.info("* DOOR " + (i + 1) + " *  ");
        }
        logger.info("\n");
     //	showObjectsBehindTheDoors();
        logger.info("\n");
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 10; j++) {
                logger.info("*         *  ");
            }
            logger.info("\n");
        }
        for (int i = 0; i < 10; i++) {
            logger.info("***********  ");
        }
    }

	private void showObjectsBehindTheDoors() {
		for (int j = 0; j < roundHall.getDoors().size(); j++) {
			if (roundHall.getDoors().get(j).getHiddenItem().toString().length() == 7) {
				logger.info("* " + roundHall.getDoors().get(j).getHiddenItem() + " *  ");
			} else
				logger.info("* " + roundHall.getDoors().get(j).getHiddenItem() + "*  ");
		}
	}

	private int calculateDeadlyDoor(int doorIndex) {
        if (doorIndex != -1) {
            HiddenItem hiddenItem = roundHall.getDoors().get(doorIndex).getHiddenItem();
            if (checkIfDeadlyDoor(hiddenItem)) {
                return 1 + calculateDeadlyDoor(doorIndex - 1);
            } else
                return 0 + calculateDeadlyDoor(doorIndex - 1);
        }
        return 0;
    }

    private boolean checkIfDeadlyDoor(HiddenItem hiddenItem){
       return hiddenItem.getTypeOfHiddenItem()==TypeOfHiddenItem.MONSTER && hiddenItem.getPower() > hero.getPower();
    }
}
package com.epam.homeworks.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Monster extends HiddenItem {
    private int power;
    private static final Logger logger = LogManager.getLogger(Monster.class);

    public Monster() {
        this.power = 5 + (int) (Math.random() * 100);
    }

    @Override
    public void show(Hero hero) {
        logger.info("Here is a monster!\n");
    }

    public void showWinner(Hero hero) {
        logger.info("Hero's power = " + hero.getPower() + ", monster's power =" + this.power);
        if (hero.getPower() >= this.power) {
            logger.info("Hero won!");
        } else
            logger.info("The monster won!");
    }

    public int getPower() {
        return power;
    }

    public String toString() {
        return "Monster";
    }

}

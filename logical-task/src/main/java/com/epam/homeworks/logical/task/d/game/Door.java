package com.epam.homeworks.logical.task.d.game;

public class Door {
    private HiddenItem hiddenItem;

    public HiddenItem getHiddenItem() {
        return hiddenItem;
    }

    private boolean isOpened;

    public Door(HiddenItem hiddenItem) {
        this.hiddenItem = hiddenItem;
    }

    public void open(Hero hero) {
        isOpened = true;
        hiddenItem.show(hero);
    }

    public boolean isOpened() {
        return isOpened;
    }
}

package com.epam.homeworks.logical.task.d.game;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        Game game = new Game();
        game.play();
    }
}

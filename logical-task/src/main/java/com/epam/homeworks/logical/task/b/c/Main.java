package com.epam.homeworks.logical.task.b.c;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static int[] removeNumberDuplicatesMoreThanTwo(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            int numberOfDuplicates = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j] && i != j) {
                    if (numberOfDuplicates == 1) {
                        array[i] = -100000;
                        count++;
                    } else
                        ++numberOfDuplicates;
                }
            }
        }
        return getInts(array, count);
    }

    private static int[] getInts(int[] array, int count) {
        int[] newArray = new int[array.length - count];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != -100000) {
                newArray[index] = array[i];
                index++;
            }
        }

        return newArray;
    }

    public static int[] removeNearbyDuplicates(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    array[i] = -100000;
                    count++;
                } else break;
            }
        }
        return getInts(array, count);
    }

    public static int[] removeNumberDuplicates(int[] array) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j] && i != j) {
                    array[i] = -100000;
                    count++;
                }
            }
        }
        return getInts(array, count);
    }

    public static void main(String[] args) {
        int[] array = new int[]{9, 9, 9, 5, 3, -22, 12, 0, 13, 13, 18, 2, 22, 22, 14, 14, 14, 8, 7, 8};
        logger.info("Old array: ");
        for (int i : array) {
            logger.info(i + " ");
        }
        logger.info("\n \nDelete all numbers which are repeated more than twice in the array.\n");
        int[] newArr = removeNumberDuplicatesMoreThanTwo(array);
        logger.info("New array: ");
        for (int i : newArr) {
            logger.info(i + " ");
        }
        logger.info("\n \nDelete nearby duplicates.\n");
        int[] newArr2 = removeNearbyDuplicates(newArr);
        logger.info("New array: ");
        for (int i : newArr2) {
            logger.info(i + " ");
        }
        logger.info("\n");
    }
}

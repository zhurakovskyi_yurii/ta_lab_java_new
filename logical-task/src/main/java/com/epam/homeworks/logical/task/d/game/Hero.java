package com.epam.homeworks.logical.task.d.game;

public class Hero {
    private String name;
    private int power;

    public Hero(String name) {
        this.name = name;
        this.power = 25;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Hero [name=" + name + ", power=" + power + "]";
    }

}

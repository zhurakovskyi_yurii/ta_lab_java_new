package com.epam.homeworks.logical.task.d.game;

public abstract class HiddenItem {
    protected TypeOfHiddenItem typeOfHiddenItem;

    public TypeOfHiddenItem getTypeOfHiddenItem() {
        return typeOfHiddenItem;
    }

    public void setTypeOfHiddenItem(TypeOfHiddenItem typeOfHiddenItem) {
        this.typeOfHiddenItem = typeOfHiddenItem;
    }

    abstract void show(Hero hero);

    abstract int getPower();
}

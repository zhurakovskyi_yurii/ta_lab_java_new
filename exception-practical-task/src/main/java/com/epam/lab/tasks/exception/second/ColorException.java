package com.epam.lab.tasks.exception.second;

public class ColorException extends Exception {
    public ColorException(String msg) {
        super(msg);
    }

    public ColorException() {
    }
}

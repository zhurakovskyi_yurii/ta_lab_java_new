package com.epam.lab.tasks.exception.second;

public class TypeException extends Exception {
    public TypeException(String msg) {
        super(msg);
    }

    public TypeException() {
    }
}

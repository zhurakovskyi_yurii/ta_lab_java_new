package com.epam.lab.tasks.exception.second;

public enum Color {
    WHITE, YELLOW, BLUE, RED, GREEN, BLACK, BROWN, AZURE, IVORY, TEAL, SILVER, PURPLE, NAVY_BLUE, PEA_GREEN, GRAY,
    ORANGE, MAROON, CHARCOAL, AQUAMARINE, CORAL, FUCHSIA, WHEAT, LIME, CRIMSON, KHAKI, HOT_PINK, MAGENTA, OLDEN, PLUM,
    OLIVE, CYAN
}

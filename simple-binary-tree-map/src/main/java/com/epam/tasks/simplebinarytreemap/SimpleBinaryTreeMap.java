package com.epam.tasks.simplebinarytreemap;

import javax.xml.crypto.dsig.keyinfo.KeyValue;
import java.util.*;

public class SimpleBinaryTreeMap<K, V>/* extends Map<K, V> */ {
    private transient Node<K, V> root = null;
    private transient int size = 0;
    private Comparator<K> comparator = null;

    public int size() {
        return size;
    }

    public V put(K key, V value) {
        if (root == null) {
            root = new Node<>(key, value, null);
            ++size;
            return null;
        } else root.put(key, value);
        return null;
    }

    public V get(K key) {
        Node<K, V> node = root.get(key);
        return node == null ? null : node.value;
    }

    public void clear() {
        size = 0;
        root = null;
    }

    private final int compare(Object key1, Object key2) {
        return comparator == null ? ((Comparable<? super K>) key1).compareTo((K) key2) : comparator.compare((K) key1, (K) key1);
    }

    private final class Node<K, V> {
        K key;
        V value;
        Node<K, V> left = null;
        Node<K, V> right = null;
        Node<K, V> parent = null;

        public Node(K key, V value, Node<K, V> parent) {
            this.key = key;
            this.value = value;
            this.parent = parent;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }

        public Node<K, V> get(K key) {
            if (this.key.equals(key))
                return this;
            if (compare(key, this.key) < 0)
                return this.left == null ? null : this.left.get(key);
            else return this.right == null ? null : this.right.get(key);
        }

        public V put(K key, V value) {
            V value1 = null;
            if (compare(key, root.key) > 0) {
                value1 = putIfPositive(key, (Node<K, V>) root, value);
            }
            return value1;
        }

        public V putIfPositive(K newKey, Node<K, V> node1, V value) {
            if (node1.right != null) {
                if (compare(key, node1.key) > 0)
                    putIfPositive(newKey, node1.right, value);
                else if (compare(key, node1.key) <= 0)
                    putIfNegative(newKey, node1.right, value);
            } else {
                node1.right = new Node<K, V>(newKey, value, node1);
            }
            return value;
        }

        private V putIfNegative(K newKey, Node<K,V> node1, V value) {
            if(node1.left != null){
                if (compare(key, node1.key) > 0)
                    putIfPositive(newKey, node1.left, value);
                else if (compare(key, node1.key) <= 0)
                    putIfNegative(newKey, node1.left, value);
            }else {
                node1.left = new Node<K, V>(newKey, value, node1);
            }
            return value;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Node)) return false;
            Node<?, ?> node = (Node<?, ?>) o;
            return Objects.equals(getKey(), node.getKey()) &&
                    Objects.equals(getValue(), node.getValue());
        }

        @Override
        public int hashCode() {
            return (key == null ? 0 : key.hashCode()) ^ (value == null ? 0 : value.hashCode());
        }

        @Override
        public String toString() {
            return "MyEntry{" +
                    "key=" + key +
                    ", value=" + value +
                    '}';
        }
    }
}

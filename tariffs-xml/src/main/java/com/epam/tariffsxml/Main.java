package com.epam.tariffsxml;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        try {
            Main main = new Main();
            File file = new File(main.getClass().getClassLoader().getResource("Tariffs.xml").getPath());
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = factory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            document.getDocumentElement().normalize();
            System.out.println("Root element :" + document.getDocumentElement().getNodeName());
            NodeList nodeList = document.getElementsByTagName("tariffs");
            System.out.println("----------------------------");
            showData(nodeList);
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

    }

    public static void showData(NodeList nodeList) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                System.out.println("name: " + element.getElementsByTagName("name").item(0).getTextContent());
                System.out.println("payroll: " + Integer.parseInt(element.getElementsByTagName("payroll").item(0).getTextContent()));
                System.out.println("internet: " + Double.parseDouble(element.getElementsByTagName("internet").item(0).getTextContent()));
                System.out.println("land-lines: " + Double.parseDouble(element.getElementsByTagName("land-lines").item(0).getTextContent()));
                System.out.println("sms-within-ukraine: " + Double.parseDouble(element.getElementsByTagName("sms-within-ukraine").item(0).getTextContent()));
            }
        }
    }
}

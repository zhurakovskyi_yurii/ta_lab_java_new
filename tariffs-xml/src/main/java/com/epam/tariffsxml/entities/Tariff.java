package com.epam.tariffsxml.entities;

import java.util.Objects;

public class Tariff {
    private String name;
    private int payroll;
    private double SmsWithinUkraine;
    private double internet;
    private double on_net;
    private double to_other_mobile;
    private double land_lines;

    public Tariff(String name, int payroll, double smsWithinUkraine, double internet, double on_net, double to_other_mobile, double land_lines) {
        this.name = name;
        this.payroll = payroll;
        SmsWithinUkraine = smsWithinUkraine;
        this.internet = internet;
        this.on_net = on_net;
        this.to_other_mobile = to_other_mobile;
        this.land_lines = land_lines;
    }

    public double getOn_net() {
        return on_net;
    }

    public void setOn_net(double on_net) {
        this.on_net = on_net;
    }

    public double getTo_other_mobile() {
        return to_other_mobile;
    }

    public void setTo_other_mobile(double to_other_mobile) {
        this.to_other_mobile = to_other_mobile;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayroll() {
        return payroll;
    }

    public void setPayroll(int payroll) {
        this.payroll = payroll;
    }

    public double getSmsWithinUkraine() {
        return SmsWithinUkraine;
    }

    public void setSmsWithinUkraine(double smsWithinUkraine) {
        SmsWithinUkraine = smsWithinUkraine;
    }

    public double getInternet() {
        return internet;
    }

    public void setInternet(double internet) {
        this.internet = internet;
    }

    public double getLand_lines() {
        return land_lines;
    }

    public void setLand_lines(double land_lines) {
        this.land_lines = land_lines;
    }

    @Override
    public String toString() {
        return "Tariff{" +
                "name='" + name + '\'' +
                ", payroll=" + payroll +
                ", SmsWithinUkraine=" + SmsWithinUkraine +
                ", internet=" + internet +
                ", on_net=" + on_net +
                ", to_other_mobile=" + to_other_mobile +
                ", land_lines=" + land_lines +
                '}';
    }
}

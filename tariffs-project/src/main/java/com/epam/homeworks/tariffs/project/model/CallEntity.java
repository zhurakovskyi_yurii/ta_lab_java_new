package com.epam.homeworks.tariffs.project.model;

import java.util.Date;

public class CallEntity {
    private int id;
    private int cuser_from;
    private int cuser_to;
    private Date date;

    public Date getDate() {
        return date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getCuser_from() {
        return cuser_from;
    }

    public void setCuser_from(int cuser_from) {
        this.cuser_from = cuser_from;
    }

    public int getCuser_to() {
        return cuser_to;
    }

    public void setCuser_to(int cuser_to) {
        this.cuser_to = cuser_to;
    }

    @Override
    public String toString() {
        return "CallEntity{" +
                "cuser_from=" + cuser_from +
                ", cuser_to=" + cuser_to +
                '}';
    }
}

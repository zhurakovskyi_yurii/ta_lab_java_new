package com.epam.homeworks.tariffs.project.controller;

import com.epam.homeworks.tariffs.project.controller.tariff.factory.TariffFactory;
import com.epam.homeworks.tariffs.project.model.*;
import com.epam.homeworks.tariffs.project.services.*;
import com.epam.homeworks.tariffs.project.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.util.Map;
import java.util.Scanner;


public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    public static Scanner scanner = new Scanner(System.in);

    public static User registration() {
        User user = new User();
        user.setAccount(new AccountEntity());
        while (true) {
            logger.info("Введіть номер телефону у форматі +380955030740: \n");
            String phone = scanner.nextLine();
            if (Controller.checkPhone(phone)) {
                logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                continue;
            } else user.getAccount().setPhone_number(phone);
            logger.info("Введіть ім'я\n");
            String firstName = scanner.nextLine().trim();
            user.setFirst_name(firstName);
            logger.info("Ім'я: " + firstName + "\n");
            logger.info("Введіть прізвище\n");
            String lastName = scanner.nextLine().trim();
            user.setLast_name(lastName);
            logger.info("Ім'я та прізвище: " + firstName + " " + lastName + "\n");
            logger.info("\nВиберіть тарифний план\n1 - Speaking, 2 - SmartphoneZero, 3 - Non_Limit_Internet\n");
            String choice = scanner.nextLine().trim();
            Tariff tariff = TariffFactory.chooseTariff(choice);
            user.setTariff(tariff);
            logger.info(firstName + " " + lastName + ", вибрано тариф " + user.getTariff().getName() + "\n");
            user.getAccount().setBalance(100);
            user.getAccount().setLast_replenishment_date(new Date(new
                    java.util.Date().getTime()));
            user.getAccount().setActive_to(new Date(new
                    java.util.Date().getTime()));
            user.getAccount().setId(1);

            AccountService accountService = new AccountService();
            accountService.create(user.getAccount());
            UserService userService = new UserService();
            userService.create(user);
            break;
        }
        return user;
    }

    public static void changeTariff(User user) {
        logger.info("\nВиберіть тарифний план\n1 - Speaking, 2 - SmartphoneZero, 3 - Non_Limit_Internet\n");
        String choice = Controller.scanner.nextLine().trim();
        Tariff tariff = TariffFactory.chooseTariff(choice);
        String name = user.getTariff().getName();
        user.setTariff(tariff);
        UserService userService = new UserService();
        userService.update(user);
        logger.info("\n Ви змінили тариф з " + name + " на: "
                + userService.findByPhone(user.getAccount().getPhone_number()).orElseThrow(IllegalArgumentException::new).getTariff().getName() + "\n");
    }

    public static void callOtherUser(User user) {
        if (user.getAccount().getBalance() > user.getTariff().getCalls_on_net_cost()) {
            double oldBalanceValue = user.getAccount().getBalance();
            double newBalanceValue = user.getAccount().getBalance() - user.getTariff().getCalls_on_net_cost();
            CallEntity callEntity = new CallEntity();
            callEntity.setCuser_from(user.getId());
            callEntity.setDate(new Date(new java.util.Date().getTime()));
            logger.info("Введіть номер отримувача: \n");
            String phone = Controller.scanner.nextLine();
            if (phone.length() != 13 || phone.charAt(0) != '+' || !isDigit(phone.substring(1))) {
                logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
            } else {
                try {
                    User user2 = new UserService().findByPhone(phone).orElseThrow(IllegalArgumentException::new);
                    callEntity.setCuser_to(user2.getId());
                    user.getAccount().setBalance(newBalanceValue);
                    logger.info("Старий баланс: " + oldBalanceValue + " Новий баланс: " + newBalanceValue + "\n");
                    AccountService accountService = new AccountService();
                    accountService.update(user.getAccount());
                    logger.info("Виклик здійснено\n");
                } catch (IllegalArgumentException e) {
                    logger.info("Такого телефону в базі немає\n");
                } catch (Exception e) {

                }
            }
            new CallService().create(callEntity);
        }
    }

    public static void sendSMS(User user) {
        if (user.getAccount().getBalance() > user.getTariff().getSms_within_ukraine_cost()) {
            logger.info("Ваше повідомлення: \n");
            String text = scanner.nextLine();
            SMS sms = new SMS();
            sms.setText(text);
            sms.setDate(new Date(new java.util.Date().getTime()));
            logger.info("\nВведіть номер отримувача: \n");
            String phone = Controller.scanner.nextLine();
            if (phone.length() != 13 || phone.charAt(0) != '+' || !isDigit(phone.substring(1))) {
                logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
            } else {
                try {
                    User user2 = new UserService().findByPhone(phone).orElseThrow(IllegalArgumentException::new);
                    sms.setUser_to(user2.getId());
                    sms.setUser_from(user.getId());
                    double oldBalanceValue = user.getAccount().getBalance();
                    double newBalanceValue = user.getAccount().getBalance() - user.getTariff().getSms_within_ukraine_cost();
                    user.getAccount().setBalance(user.getAccount().getBalance() - user.getTariff().getSms_within_ukraine_cost());
                    new AccountService().update(user.getAccount());
                    logger.info("Старий баланс: " + oldBalanceValue + " Новий баланс: " + newBalanceValue + "\n");
                    new SMSService().create(sms);
                    logger.info("Відправлено\n");
                } catch (IllegalArgumentException e) {
                    logger.info("\nТакого телефону в базі немає\n");
                }
            }
        }
    }

    public static boolean isDigit(String substring) {
        for (char c : substring.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    public static void createTariffs() {
        TariffService tariffService = new TariffService();
        if (!tariffService.findByName("Speaking").isPresent())
            tariffService.create(new Speaking());
        if (!tariffService.findByName("Smartphone Zero").isPresent())
            tariffService.create(new SmartphoneZero());
        if (!tariffService.findByName("Non-Limit Internet").isPresent())
            tariffService.create(new Non_Limit_Internet());
    }

    public static User authentication() {
        User user = new User();
        while (true) {
            AccountEntity accountEntity = new AccountEntity();
            logger.info("Введіть номер телефону у форматі +380955030740: \n");
            String phone = Controller.scanner.nextLine().trim();
            if (phone.length() != 13 || phone.charAt(0) != '+' || !isDigit(phone.substring(1))) {
                logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                continue;
            } else {
                AccountService accountService = new AccountService();
                accountEntity = accountService.findByPhone(phone).orElseThrow(IllegalArgumentException::new);
            }
            UserService userService = new UserService();
            user = userService.findByPhone(phone).orElseThrow(IllegalArgumentException::new);
            user.setAccount(accountEntity);
            logger.info(user.getFirst_name() + " " + user.getLast_name() + " " + user.getAccount().getPhone_number());
            break;
        }
        return user;
    }

    public static void start() {
        boolean exit = false;
        User user = new User();
        while (!exit) {
            createTariffs();
            View view = new View();
            view.view();
            for (Map.Entry<String, String> entry : View.menu.entrySet()) {
                logger.info(entry.getKey() + " " + entry.getValue() + "\n");
            }
            String choice = scanner.nextLine().trim().toUpperCase();
            switch (choice) {
                case "1":
                    user = registration();
                    break;
                case "2":
                    changeTariff(user);
                    break;
                case "3":
                    logger.info("Введіть номер телефону у форматі +380955030740: \n");
                    String phone = scanner.nextLine();
                    if (checkPhone(phone)) {
                        logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                    } else {
                        new UserService().deleteByPhone(phone);
                        if(!new UserService().findByPhone(phone).isPresent())
                            logger.info("Користувача видалено!");
                    }
                    break;
                case "4":
                    //replenishment(User user);
                    break;
                case "5":
                    break;
                case "6":
                    sendSMS(user);
                    break;
                case "EXIT":
                    exit = true;
                    break;
                default:
                    logger.info("\nНевірний вибір\n");
            }
        }
    }

    public static boolean checkPhone(String phone) {
        return phone.length() != 13 || phone.charAt(0) != '+' || !isDigit(phone.substring(1));
    }

   /* public static void main(String[] args) {

        String choiceS = "";
        while (true) {
            logger.info("\n1 Реєстрація\n");
            logger.info("2 Вхід\n");
            logger.info("exit Закрити програму\n");
            try {
                choiceS = scanner.nextLine();
                int choice = Integer.parseInt(choiceS);
                if (choice == 1)
                    registration();
                else if (choice == 2) {
                    User user = Authentication.authentication();
                    logger.info("\n1 Відправити смс\n");
                    logger.info("2 Подзвонити\n");
                    logger.info("3 Змінити тариф\n");
                    int _choice = Integer.parseInt(scanner.nextLine());
                    if (_choice == 1) {
                        logger.info("Ваше повідомлення: \n");
                        String text = scanner.nextLine();
                        Services.sendSMS(user, text);
                    } else if (_choice == 2) {
                        Services.callOtherUser(user);
                    } else if (_choice == 3) {
                        Services.changeTariff(user);
                    }
                }
            } catch (NumberFormatException e) {
                if (choiceS.equalsIgnoreCase("exit"))
                    break;
                else logger.info("\nНекоректний ввід\n");
            }
        }
    }*/


}

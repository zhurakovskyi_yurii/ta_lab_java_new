package com.epam.homeworks.tariffs.project.model;

public class User {
    private int id;
    private AccountEntity account;
    private String first_name;
    private String last_name;
    private Tariff tariff;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AccountEntity getAccount() {
        return account;
    }

    public void setAccount(AccountEntity account) {
        this.account = account;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public Tariff getTariff() {
        return tariff;
    }

    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", account=" + account +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", tariff=" + tariff +
                '}';
    }
}

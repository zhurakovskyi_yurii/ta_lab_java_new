package com.epam.homeworks.tariffs.project.controller.tariff.factory;

import com.epam.homeworks.tariffs.project.model.Non_Limit_Internet;
import com.epam.homeworks.tariffs.project.model.SmartphoneZero;
import com.epam.homeworks.tariffs.project.model.Speaking;
import com.epam.homeworks.tariffs.project.model.Tariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TariffFactory {
    private static Logger logger = LogManager.getLogger(TariffFactory.class);
    public static Tariff chooseTariff(String choice) {
        switch (choice) {
            case "1":
                return new Speaking();
            case "2":
                return new SmartphoneZero();
            case "3":
                return new Non_Limit_Internet();
            default:
                logger.info("Невірний вибір\n");
                break;
        }
        return new Speaking();
    }
}

package com.epam.homeworks.tariffs.project.dao;


import java.sql.SQLException;
import java.util.List;

public interface GeneralDAO<T> {
    List<T> findAll() throws SQLException;

    void create(T entity) throws SQLException;
}

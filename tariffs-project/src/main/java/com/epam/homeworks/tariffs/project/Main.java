package com.epam.homeworks.tariffs.project;


import com.epam.homeworks.tariffs.project.controller.Controller;

public class Main {
    public static void main(String[] args) {
        Controller.start();
    }
}

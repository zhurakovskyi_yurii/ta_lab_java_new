package com.epam.homeworks.tariffs.project.model;

public class SmartphoneZero extends Tariff {
    public SmartphoneZero() {
        this.setId(2);
        this.setName("Smartphone Zero");
        this.setPayroll(120);
        this.setCalls_on_net_cost(1.20);
        this.setSms_within_ukraine_cost(0.50);
        this.setInternet(1000);
        this.setLand_lines(2.50);
    }
}

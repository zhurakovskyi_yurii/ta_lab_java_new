package com.epam.regex;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex_Task {
    public static final String WORD = "[a-zA-z0-9_]+";
    public static final String SENTENCE = "[^.!?\\s][^.!?]*(?:[.!?](?!['\\\"]?)\\s|$)";// not correct
    public static final String PUNCTUATION_MARK = "";
    private static final Logger logger = LogManager.getLogger(Regex_Task.class);

    public static void main(String[] args) {
        logger.info(isWord("edrtg"));
        logger.info(isSentence("Edrtg dfrtgy derftg"));
    }

    public static boolean isWord(String string){
        return match( WORD, string);
    }

    public static boolean isSentence(String string){
        return match( SENTENCE, string);
    }

    public static boolean isPunctuationMark(String string){
        return match( PUNCTUATION_MARK, string);
    }

    public static boolean match(String pattern, String string){
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(string);
        return m.matches();
    }
}

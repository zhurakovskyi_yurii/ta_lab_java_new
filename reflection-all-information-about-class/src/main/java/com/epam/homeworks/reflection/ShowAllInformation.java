package com.epam.homeworks.reflection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.*;

public class ShowAllInformation {
    private static final Logger logger = LogManager.getLogger(ShowAllInformation.class);

    public static void showAllInformation(Object object) {
        Class clazz = object.getClass();
        logger.info(clazz.getSimpleName() + "\n");
        String clazzPackage = clazz.getPackage().toString();
        logger.info(clazzPackage + "\n");
        Constructor[] constructors = clazz.getConstructors();
        logger.info("Constructors: " + "\n");
        for (Constructor constructor : constructors) {
            logger.info(constructor + "\n");
        }
        logger.info("Declared Fields: " + "\n");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            logger.info(field + "\n");
        }
        Class[] classes = clazz.getClasses();
        logger.info("Subclasses: \n");
        for (Class c : classes) {
            logger.info(c + "\n");
        }
        logger.info("Canonical Name: \n");
        logger.info(clazz.getCanonicalName() + "\n");
        logger.info("Interfaces: \n");
        Class[] clazzes = clazz.getInterfaces();
        for (Class c : clazzes) {
            logger.info(c + "\n");
        }
        logger.info("Generic Superclass: \n");
        logger.info(clazz.getGenericSuperclass() + "\n");
        logger.info("Name of class: \n");
        logger.info(clazz.getName() + "\n");
        Method[] methods = clazz.getMethods();
        logger.info("Methods: \n");
        for (Method method : methods) {
            logger.info(method + "\n");
        }
        logger.info("Generic interfaces: \n");
        Type[] genericInterfaces = clazz.getGenericInterfaces();
        for (Type type : genericInterfaces) {
            logger.info(type + "\n");
        }
        Class c = clazz.getSuperclass();
        logger.info("Super class: \n");
        logger.info(c + "\n");
        logger.info("Type parameters: \n");
        TypeVariable[] typeParameters = clazz.getTypeParameters();
        for (TypeVariable v : typeParameters) {
            logger.info(v + "\n");
        }
        logger.info("Modifier: \n");
        int modifier = clazz.getModifiers();
        logger.info(Modifier.toString(modifier));
    }
}

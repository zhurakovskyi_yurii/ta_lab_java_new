package com.epam.homeworks.reflection;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put("A", "A");
        treeMap.put("B", "A");
        treeMap.put("C", "A");
        treeMap.put("D", "A");
        treeMap.put("E", "A");
        Map<String, String> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("A", "A");
        linkedHashMap.put("B", "A");
        linkedHashMap.put("C", "A");
        linkedHashMap.put("D", "A");
        linkedHashMap.put("E", "A");
        List<Integer> list = new ArrayList<>();
        list.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7));
        ShowAllInformation.showAllInformation(new ShowAllInformation());
    }
}

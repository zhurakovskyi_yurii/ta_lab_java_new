package com.epam.lab.hypermarket.householdappliancesandconsumerelectronics;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

/**
 * @author Yurii Zhurakovskyi
 */
public class SmartTV extends Good {

	private String screen_size;
	private String display_format;
	private String wide_screen;
	private String backlight;
	private String display;

	public SmartTV(String name, Country country, Brand brand, double price, String screen_size, String display_format,
			String wide_screen, String backlight, String display) {
		super(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, name, country, price, brand);
		this.screen_size = screen_size;
		this.display_format = display_format;
		this.wide_screen = wide_screen;
		this.backlight = backlight;
		this.display = display;
	}

	public String getScreen_size() {
		return screen_size;
	}

	public void setScreen_size(String screen_size) {
		this.screen_size = screen_size;
	}

	public String getDisplay_format() {
		return display_format;
	}

	public void setDisplay_format(String display_format) {
		this.display_format = display_format;
	}

	public String getWide_screen() {
		return wide_screen;
	}

	public void setWide_screen(String wide_screen) {
		this.wide_screen = wide_screen;
	}

	public String getBacklight() {
		return backlight;
	}

	public void setBacklight(String backlight) {
		this.backlight = backlight;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((backlight == null) ? 0 : backlight.hashCode());
		result = prime * result + ((display == null) ? 0 : display.hashCode());
		result = prime * result + ((display_format == null) ? 0 : display_format.hashCode());
		result = prime * result + ((screen_size == null) ? 0 : screen_size.hashCode());
		result = prime * result + ((wide_screen == null) ? 0 : wide_screen.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmartTV other = (SmartTV) obj;
		if (backlight == null) {
			if (other.backlight != null)
				return false;
		} else if (!backlight.equals(other.backlight))
			return false;
		if (display == null) {
			if (other.display != null)
				return false;
		} else if (!display.equals(other.display))
			return false;
		if (display_format == null) {
			if (other.display_format != null)
				return false;
		} else if (!display_format.equals(other.display_format))
			return false;
		if (screen_size == null) {
			if (other.screen_size != null)
				return false;
		} else if (!screen_size.equals(other.screen_size))
			return false;
		if (wide_screen == null) {
			if (other.wide_screen != null)
				return false;
		} else if (!wide_screen.equals(other.wide_screen))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", screen size: " + screen_size + ", display format: " + display_format
				+ ", wide screen: " + wide_screen + ", backlight: " + backlight + ", display" + display;
	}
}

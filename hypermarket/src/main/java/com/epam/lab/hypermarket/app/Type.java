package com.epam.lab.hypermarket.app;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public enum Type {
	WOOD_PRODUCTS, PLUMBING, PAINTS_AND_VARNISHES, CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, BUILDING_MATERIAL
}

package com.epam.lab.hypermarket.app;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public enum Country {
	FRANCE, FINLAND, BELGIUM, AUSTRALIA, CANADA, AUSTRIA, GERMANY
}

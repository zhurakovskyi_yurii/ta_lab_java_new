package com.epam.lab.hypermarket.app;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public abstract class HypermarketEntity {
	private String name;
	private Map<Integer, Good> goods;

	public HypermarketEntity() {
		name = "My Hypermarket";
		goods = new TreeMap<>();
	}

	public HypermarketEntity(String name) {
		this.name = name;
		goods = new TreeMap<>();
	}

	public HypermarketEntity(String name, TreeMap<Integer, Good> goods) {
		this.name = name;
		this.goods = goods;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Map<Integer, Good> getGoods() {
		return goods;
	}

	protected void addGood(Good good) {
		goods.put(getNewID(good), good);
	}

	private int getNewID(Good good) {
		int id = -1;
		while (id == -1) {
			int tmpId = (good.getType().ordinal() + 1) * 10000 + (int) (Math.random() * 10000);
			if (!goods.keySet().stream().filter(key -> key == tmpId).findAny().isPresent())
				id = tmpId;
		}
		good.setId(id);
		return id;
	}

	protected void removeGood(int key) {
		goods.remove(key);
	}

	private String goodsList() {
		String _s = "";
		for (Map.Entry<Integer, Good> entry : goods.entrySet()) {
			_s += ("\n" + entry.getKey() + ": " + entry.getValue() + "\n");
		}
		return _s;
	}

	@Override
	public String toString() {
		return name + ": \n" + goodsList();
	}
}

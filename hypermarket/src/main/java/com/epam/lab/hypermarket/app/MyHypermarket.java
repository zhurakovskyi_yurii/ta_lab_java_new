package com.epam.lab.hypermarket.app;

import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * @author Yurii Zhurakovskyi
 *
 */
public class MyHypermarket extends HypermarketEntity {
	private static MyHypermarket instance;

	private MyHypermarket() {
	}

	public static MyHypermarket getInstance() {
		if (instance == null)
			instance = new MyHypermarket();
		return instance;
	}

	public Collection<Good> findByTypeAndPriceLessThanOrEqual(String type, int price) {
		Collection<Good> goods = new TreeSet<Good>(Comparator.comparing(Good::getId));
		if (instance != null) {
			goods.addAll(instance.getGoods().values().stream()
					.filter(g -> g.getType().name().equalsIgnoreCase(type) && g.getPrice() <= price)
					.collect(Collectors.toSet()));
		} else
			System.out.println("MyHypermarket is empty");

		return goods;
	}

	public Collection<Good> findByTypeAndPriceMoreThanOrEqual(String type, int price) {
		Collection<Good> goods = new TreeSet<Good>(Comparator.comparing(Good::getId));
		if (instance != null) {
			goods.addAll(instance.getGoods().values().stream()
					.filter(g -> g.getType().name().equalsIgnoreCase(type) && g.getPrice() >= price)
					.collect(Collectors.toSet()));
		} else
			System.out.println("MyHypermarket is empty");
		return goods;
	}

	public Collection<Good> findByBrandAndType(String type, String brandName) {
		Collection<Good> goods = new TreeSet<Good>(Comparator.comparing(Good::getId));
		if (instance != null) {
			goods.addAll(instance.getGoods().values().stream()
					.filter(g -> g.getType().name().equalsIgnoreCase(type) && g.getBrand().getName().equalsIgnoreCase(brandName))
					.collect(Collectors.toSet()));
		} else
			System.out.println("MyHypermarket is empty");
		return goods;
	}
}

package com.epam.lab.hypermarket.householdappliancesandconsumerelectronics;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Material;
import com.epam.lab.hypermarket.app.Type;

public class FoodProcessor extends Good {

	private int weight;
	private Material material;
	private int width;
	private int height;
	private int depth;
	private int size;

	public FoodProcessor(String name, Country country, Brand brand, double price, int weight, Material material,
			int width, int height, int depth, int size) {
		super(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, name, country, price, brand);
		this.weight = weight;
		this.material = material;
		this.width = width;
		this.depth = depth;
		this.height = height;
		this.size = size;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Material getMaterial() {
		return material;
	}

	public void setMaterial(Material material) {
		this.material = material;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getDepth() {
		return depth;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + depth;
		result = prime * result + height;
		result = prime * result + ((material == null) ? 0 : material.hashCode());
		result = prime * result + size;
		result = prime * result + weight;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		FoodProcessor other = (FoodProcessor) obj;
		if (depth != other.depth)
			return false;
		if (height != other.height)
			return false;
		if (material != other.material)
			return false;
		if (size != other.size)
			return false;
		if (weight != other.weight)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", weight: " + weight + ", material: " + material + ", width: " + width + ", height: "
				+ height + ", depth: " + depth + ", size: " + size + "-Cup";
	}

}

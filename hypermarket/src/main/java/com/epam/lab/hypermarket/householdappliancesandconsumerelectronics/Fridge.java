package com.epam.lab.hypermarket.householdappliancesandconsumerelectronics;

import com.epam.lab.hypermarket.app.Brand;
import com.epam.lab.hypermarket.app.Color;
import com.epam.lab.hypermarket.app.Country;
import com.epam.lab.hypermarket.app.Good;
import com.epam.lab.hypermarket.app.Type;

public class Fridge extends Good {

	private int capacity;
	private int voltage;
	private Color color;
	private String shape;
	private int weight;
	private int width;
	private int height;
	private int depth;

	public Fridge(String name, Country country, Brand brand, double price, int capacity, int voltage, Color color,
			String shape, int weight, int width, int height, int depth) {
		super(Type.CONSUMER_ELECTRONICS_AND_HOUSEHOLD_APPLIANCES, name, country, price, brand);
		this.capacity = capacity;
		this.voltage = voltage;
		this.color = color;
		this.shape = shape;
		this.weight = weight;
		this.width = width;
		this.height = height;
		this.depth = depth;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setVoltage(int voltage) {
		this.voltage = voltage;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public void setShape(String shape) {
		this.shape = shape;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public void setDepth(int depth) {
		this.depth = depth;
	}

	public int getCapacity() {
		return capacity;
	}

	public int getVoltage() {
		return voltage;
	}

	public Color getColor() {
		return color;
	}

	public String getShape() {
		return shape;
	}

	public int getWeight() {
		return weight;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int getDepth() {
		return depth;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + capacity;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result + depth;
		result = prime * result + height;
		result = prime * result + ((shape == null) ? 0 : shape.hashCode());
		result = prime * result + voltage;
		result = prime * result + weight;
		result = prime * result + width;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fridge other = (Fridge) obj;
		if (capacity != other.capacity)
			return false;
		if (color != other.color)
			return false;
		if (depth != other.depth)
			return false;
		if (height != other.height)
			return false;
		if (shape == null) {
			if (other.shape != null)
				return false;
		} else if (!shape.equals(other.shape))
			return false;
		if (voltage != other.voltage)
			return false;
		if (weight != other.weight)
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return super.toString() + ", color: " + color + ", " + height + "X" + width + "X" + depth + ", capacity: "
				+ capacity + " litres, weight: " + weight + " Kg, Voltage: " + voltage + " V, shape: " + shape;
	}
}

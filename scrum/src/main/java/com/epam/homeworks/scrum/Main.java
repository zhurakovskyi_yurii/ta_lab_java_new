package com.epam.homeworks.scrum;

import com.epam.homeworks.scrum.entity.Task;
import com.epam.homeworks.scrum.state.ProductBacklog;

public class Main {
    public static void main(String[] args) {
        Task task = new Task(ProductBacklog.getInstance());
        task.update();
        task.update();
        task.block();
        task.update();
        task.update();
        task.update();
        task.update();
    }
}

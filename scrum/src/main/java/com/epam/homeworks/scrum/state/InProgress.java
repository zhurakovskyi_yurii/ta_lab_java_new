package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InProgress implements State {
    private static Logger logger = LogManager.getLogger(InProgress.class);
    private static InProgress instance = new InProgress();

    private InProgress(){}
    public static InProgress getInstance() {
        return instance;
    }

    public void updateState(Task task) {
        logger.info("In progress\n");
        task.setState(PeerReview.getInstance());
    }
    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

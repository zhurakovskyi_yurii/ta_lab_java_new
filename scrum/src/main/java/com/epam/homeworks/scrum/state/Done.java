package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Done implements State {
    private static Logger logger = LogManager.getLogger(Done.class);
    private static Done instance = new Done();

    private Done(){}

    public static Done getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("Done\n");
    }

    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

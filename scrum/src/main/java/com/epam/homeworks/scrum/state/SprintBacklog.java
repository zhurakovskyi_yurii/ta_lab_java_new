package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SprintBacklog implements State {
    private static Logger logger = LogManager.getLogger(SprintBacklog.class);
    private static SprintBacklog instance = new SprintBacklog();

    private SprintBacklog(){}
    public static SprintBacklog getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("Sprint backlog\n");
        task.setState( InProgress.getInstance());
    }
    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

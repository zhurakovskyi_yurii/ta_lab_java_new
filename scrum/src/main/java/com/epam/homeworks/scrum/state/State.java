package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;

public interface State {
    void updateState(Task task);
    void blockTask(Task task);
}

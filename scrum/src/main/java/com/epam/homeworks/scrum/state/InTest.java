package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InTest implements State {
    private static Logger logger = LogManager.getLogger(InTest.getInstance());
    private static InTest instance = new InTest();

    private InTest(){}
    public static InTest getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("In test\n");
        task.setState(Done.getInstance());
    }
    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

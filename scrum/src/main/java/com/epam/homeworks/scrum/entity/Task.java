package com.epam.homeworks.scrum.entity;

import com.epam.homeworks.scrum.state.State;

public class Task {
    private State state;

    public Task(State state) {
        this.state = state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void update() {
        state.updateState(this);
    }

    public void block() {
        state.blockTask(this);
    }
}

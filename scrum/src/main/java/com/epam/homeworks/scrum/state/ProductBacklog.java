package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ProductBacklog implements State {
    private static Logger logger = LogManager.getLogger(ProductBacklog.class);
    private static ProductBacklog instance = new ProductBacklog();

    private ProductBacklog(){}
    public static ProductBacklog getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("Product Backlog\n");
        task.setState(SprintBacklog.getInstance());
    }
    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

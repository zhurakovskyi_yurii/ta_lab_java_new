package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class PeerReview implements State {
    private static Logger logger = LogManager.getLogger(PeerReview.class);
    private static PeerReview instance = new PeerReview();

    private PeerReview() { }

    public static PeerReview getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("Peer review\n");
        task.setState(InTest.getInstance());
    }
    public void blockTask(Task task){
        task.setState(Blocked.getInstance());
    }
}

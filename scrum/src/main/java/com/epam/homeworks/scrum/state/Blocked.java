package com.epam.homeworks.scrum.state;

import com.epam.homeworks.scrum.entity.Task;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Blocked implements State {
    private static Logger logger = LogManager.getLogger(Blocked.class);
    private static Blocked instance = new Blocked();

    private Blocked(){}

    public static Blocked getInstance() {
        return instance;
    }

    @Override
    public void updateState(Task task) {
        logger.info("Blocked\n");
    }

    @Override
    public void blockTask(Task task) {
        logger.info("Blocked\n");
    }
}

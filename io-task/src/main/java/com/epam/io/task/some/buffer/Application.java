package com.epam.io.task.some.buffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        SomeBuffer someBuffer = new SomeBuffer("io-task\\nio-test1.txt");
        someBuffer.writeToBuffer("Some text");
        someBuffer.readFromBuffer();
        someBuffer.printText();
    }
}

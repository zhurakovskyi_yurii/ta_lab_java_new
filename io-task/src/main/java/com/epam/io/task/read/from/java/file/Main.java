package com.epam.io.task.read.from.java.file;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        CommentReader commentReader = new CommentReader("io-task\\Application.java");
        commentReader.readFromFile();
        commentReader.saveOneLineComments();
        commentReader.printComments();
    }
}

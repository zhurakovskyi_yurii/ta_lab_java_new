package com.epam.io.task.display.content;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DisplayContent {
    private String path;
    private File file;
    private static Logger logger = LogManager.getLogger(DisplayContent.class);

    public DisplayContent() {
        path = "";
    }

    public void run() {
        if (path != null)
            logger.info("\n" + path + "\n");
        try (InputStreamReader inputStreamReader = new InputStreamReader(System.in);
             BufferedReader bufferedReader = new BufferedReader(inputStreamReader);) {
            String input = "";
            while (!input.equals("exit")) {
                if (path != null) {
                    logger.info("\n" + path + " ");
                }
                input = bufferedReader.readLine();
                checkInput(input);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void checkInput(String input) throws IOException {
        input = input.trim();
        if (input.equals("cd") && path != "" && new File(path).isDirectory()) {
            logger.info(path + "\n");
        } else if (input.equals("dir") && path != "") {
            if (file.isDirectory()) {
                showDirectoryContent();
            } else logger.info("Системі не вдається знайти шлях\n");
        } else {
            int firstIndex = 0;
            boolean action = true;
            if (input.contains("cd"))
                firstIndex = 2;
            else if (input.contains("dir")) {
                firstIndex = 3;
                action = false;
            } else logger.info("Системі не вдається знайти шлях\n");
            char[] chars = input.toCharArray();
            String tmp = "";
            for (int i = firstIndex + 1; i < chars.length; i++) {
                tmp += chars[i];
            }
            if (new File(tmp).isDirectory()) {
                path = tmp;
                file = new File(path);
            }
            if (action && file.isDirectory()) {
                logger.info(path + "\n");
            }

            if (!action) {
                if (file.isDirectory()) {
                    showDirectoryContent();
                } else logger.info("Системі не вдається знайти шлях\n");
            }
        }
    }

    private void showDirectoryContent() throws IOException {
        File[] files = file.listFiles();
        for (File file1 : files) {
            String date = String.valueOf(Files.getAttribute(Paths.get(file1.getPath()), "creationTime"));
            if (file1.isDirectory()) {
                logger.info(date + " <dir> " + file1.getName() + "\n");
            } else
                logger.info(date + " " + (double) file1.length() / 1024 + " " + file1.getName() + "\n");
        }
    }
}

package com.epam.io.task.some.buffer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class SomeBuffer {
    private RandomAccessFile aFile;
    private FileChannel fileChannel;
    private ByteBuffer buf;
    private String text;
    private static Logger logger = LogManager.getLogger(SomeBuffer.class);

    SomeBuffer(String fileName) {
        try {
            text = "";
            aFile = new RandomAccessFile(fileName, "rw");
            buf = ByteBuffer.allocate(48);
            fileChannel = aFile.getChannel();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readFromBuffer() {
        try {
            int bytesRead = fileChannel.read(buf);
            while (bytesRead != -1) {
                buf.flip();
                while (buf.hasRemaining()) {
                    text += (char) buf.get();
                }
                buf.clear();
                bytesRead = fileChannel.read(buf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeToBuffer(String data) {
        try {
            buf = ByteBuffer.allocate(48);
            buf.clear();
            buf.put(data.getBytes());
            buf.flip();
            while (buf.hasRemaining()) {
                fileChannel.write(buf);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printText() {
        logger.info(text + "\n");
    }
}
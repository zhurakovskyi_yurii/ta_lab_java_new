package com.epam.io.task.read.from.java.file;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CommentReader {
    private List<String> text;
    private List<String> comments;
    private String filePath;
    private static Logger logger = LogManager.getLogger(CommentReader.class);

    public CommentReader(String filePath) {
        this.text = new ArrayList<>();
        this.comments = new ArrayList<>();
        this.filePath = filePath;
    }

    public void readFromFile() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String s = "";
            while ((s = bufferedReader.readLine()) != null) {
                s = s.trim();
                text.add(s);
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public List<String> getText() {
        return text;
    }

    public void saveOneLineComments() {
        boolean b = false;
        boolean foundFirstType = false;
        boolean foundSecondType = false;
        String comment = "";
        for (int i = 0; i < text.size(); i++) {
            char[] chars = text.get(i).toCharArray();
            for (int j = 0; j < chars.length; j++) {
                if (foundFirstType) {
                    comment += chars[j];
                } else if (foundSecondType && (chars[j] != '*' && chars[j + 1] != '/')) {
                    comment += chars[j];
                } else foundSecondType = false;
                if (chars[j] == '/' && j+1 < chars.length) {
                    if (chars[j + 1] == '/') {
                        foundFirstType = true;
                        j++;
                    } else if (chars[j + 1] == '*') {
                        foundSecondType = true;
                        j++;
                    }
                }
            }
            foundFirstType = false;
            comment = comment.trim();
            if(comment != "") {
                comments.add(comment);
                comment = "";
            }
        }
    }

    public void printComments() {
        comments.forEach(c -> logger.info("\n" + c + "\n"));
    }
}
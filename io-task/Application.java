package com.epam.io.task.reading.using.reader.and.buffered.reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Application {
    private static Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args) {
    }

    //Read from 200MB file using BufferedReader
    public static void readFromTwoHundredMbFileUsingBufferedReader() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("io-task\\test.txt"))) {
            String s = "";
            while ((s = bufferedReader.readLine()) != null) {
                logger.info(s + "\n");
                //Read from 200MB file using BufferedReader
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /*Read from 200MB file using FileReader */
    public static void readFromTwoHundredMbFileUsingFileReader() {
        try {
            Reader fileReader = new FileReader("io-task\\test.txt");//Read from 200MB file using BufferedReader
            int data = fileReader.read();
            while (data != -1) {
                if ((char) data == '.')
                    /*Read from 200MB
                    file using FileReader */
                    logger.info("\n");
                logger.info(((char) data));
                data = fileReader.read();
            }
            logger.info("\n");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToFileUsingBufferedInputStream() {
        String s = "Some String";
        try (FileOutputStream fileOutputStream = new FileOutputStream("io-task\\test1.txt");
        BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream)) {
            byte[] buffer = s.getBytes();
            bos.write(buffer, 0, buffer.length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

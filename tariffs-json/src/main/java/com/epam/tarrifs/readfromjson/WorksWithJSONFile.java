package com.epam.tarrifs.readfromjson;

import com.epam.tarrifs.TariffsComparator;
import com.epam.tarrifs.entities.Callprices;
import com.epam.tarrifs.entities.Tariff;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;

public class WorksWithJSONFile {
    public static void readTarrifsFromFile() throws FileNotFoundException {
        String resourceName = "/tariffs.json";
        InputStream is = WorksWithJSONFile.class.getResourceAsStream(resourceName);
        if (is == null) {
            throw new FileNotFoundException("Cannot find resource file " + resourceName);
        }
        JSONTokener jsonTokener = new JSONTokener(is);
        JSONArray jsonArray = new JSONArray(jsonTokener);
        Set<Tariff> tariffs = new TreeSet<>(Comparator.comparing(Tariff::getName));
        jsonArray.forEach(t -> tariffs.add(getTariffObject((JSONObject) t)));
        System.out.println();
        showAllTariffs(tariffs);
        System.out.println();
        Set<Tariff> tariffs1 = new TreeSet<>(new TariffsComparator());
        jsonArray.forEach(t -> tariffs.add(getTariffObject((JSONObject) t)));
        System.out.println();
        showAllTariffs(tariffs);
        List<Tariff> tariffs2 = new ArrayList<>();
        jsonArray.forEach(t -> tariffs.add(getTariffObject((JSONObject) t)));
        System.out.println();
        tariffs2.sort(new TariffsComparator());
        showAllTariffs(tariffs);
    }

    public static Tariff getTariffObject(JSONObject jsonObject){
        System.out.println();
        String name = (String) jsonObject.get("name");
        System.out.println(name);
        int payroll = Integer.parseInt(String.valueOf(jsonObject.get("payroll")));
        System.out.println("payroll: " + payroll);
        JSONObject jsonObject12 = (JSONObject) jsonObject.get("call-prices");
        System.out.println("callprices: ");
        double on_net = Double.parseDouble((String) jsonObject12.get("on-net"));
        System.out.println(" on-net: " + on_net);
        double to_other_mobile = Double.parseDouble((String) jsonObject12.get("to-other-mobile"));
        System.out.println(" to-other-mobile: " + to_other_mobile);
        double SmsWithinUkraine = Double.parseDouble((String) jsonObject.get("sms-within-ukraine"));
        System.out.println("sms within ukraine: " + SmsWithinUkraine);
        double internet = Double.parseDouble((String) jsonObject.get("internet"));
        System.out.println("internet: " + internet);
        double land_lines = Double.parseDouble((String) jsonObject.get("land-lines"));
        System.out.println("land lines: " + land_lines);
        return new Tariff(name, payroll, new Callprices(on_net, to_other_mobile), SmsWithinUkraine, internet, land_lines);
    }

    public static void showAllTariffs(Collection<Tariff> tariffs){
        tariffs.forEach(t -> System.out.println(t));
    }

}

package com.epam.tarrifs.readfromjson;

import com.epam.tarrifs.entities.Callprices;
import com.epam.tarrifs.entities.Tariff;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public class GSONReading {
    public static void main(String[] args) {
        String resourceName = "/tariff.json";
        Gson gson = new GsonBuilder().create();
        try (Reader reader = new FileReader(GSONReading.class.getResource(resourceName).getPath())){
            Tariff tariff = gson.fromJson(reader, Tariff.class);
            System.out.println(tariff);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
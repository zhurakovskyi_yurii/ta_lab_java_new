package com.epam.tarrifs.readfromjson;

import com.epam.tarrifs.entities.Callprices;
import com.epam.tarrifs.entities.Tariff;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class ReadUsingMapper {
    public static Tariff readUsingMapperTariff() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        String resourceName = "/tariff.json";
        return mapper.readValue(ReadUsingMapper.class.getResource(resourceName), Tariff.class);
    }

    public static Callprices readUsingMapperCallprices() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String resourceName = "/tariff.json";
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        Callprices callprices = mapper.readValue(ReadUsingMapper.class.getResource(resourceName), Callprices.class);
        return callprices;
    }

    public static void main(String[] args) {
        try {
            Tariff tariff = readUsingMapperTariff();
            tariff.setCallprices(readUsingMapperCallprices());
            System.out.println(tariff);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

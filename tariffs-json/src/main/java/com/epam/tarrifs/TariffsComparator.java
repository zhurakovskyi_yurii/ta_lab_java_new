package com.epam.tarrifs;

import com.epam.tarrifs.entities.Tariff;

import java.util.Comparator;

public class TariffsComparator implements Comparator<Tariff> {
    @Override
    public int compare(Tariff o1, Tariff o2) {
        if (o1.getPayroll() > o2.getPayroll())
            return 1;
        if(o1.getPayroll() < o2.getPayroll())
            return -1;
        if (o1.getName().compareTo(o2.getName())> 0)
            return 1;
        if (o1.getName().compareTo(o2.getName()) < 0)
            return -1;
        if(o1.getCallprices().getOn_net() > o1.getCallprices().getOn_net())
            return 1;
        if(o1.getCallprices().getTo_other_mobile() > o1.getCallprices().getTo_other_mobile())
            return -1;
        if (o1.getSmsWithinUkraine() > o1.getSmsWithinUkraine())
            return 1;
        if (o1.getSmsWithinUkraine() < o1.getSmsWithinUkraine())
            return -1;
        if(o1.getInternet() > o1.getInternet())
            return 1;
        if (o1.getInternet() < o1.getInternet())
            return -1;
        if (o1.getLand_lines() > o1.getLand_lines())
            return 1;
        if (o1.getLand_lines() < o1.getLand_lines())
            return -1;
        return 0;
    }
}

package com.epam.tarrifs.entities;

import java.util.Objects;

public class Tariff {
    private String name;
    private int payroll;
    private Callprices callprices;
    private double SmsWithinUkraine;
    private double internet;
    private double land_lines;

    public Tariff() {
        callprices = null;
    }

    public Tariff(String name, int payroll, Callprices callprices, double smsWithinUkraine, double internet, double land_lines) {
        this.name = name;
        this.payroll = payroll;
        this.callprices = callprices;
        SmsWithinUkraine = smsWithinUkraine;
        this.internet = internet;
        this.land_lines = land_lines;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPayroll() {
        return payroll;
    }

    public void setPayroll(int payroll) {
        this.payroll = payroll;
    }

    public Callprices getCallprices() {
        return callprices;
    }

    public void setCallprices(Callprices callprices) {
        this.callprices = callprices;
    }

    public double getSmsWithinUkraine() {
        return SmsWithinUkraine;
    }

    public void setSmsWithinUkraine(double smsWithinUkraine) {
        SmsWithinUkraine = smsWithinUkraine;
    }

    public double getInternet() {
        return internet;
    }

    public void setInternet(double internet) {
        this.internet = internet;
    }

    public double getLand_lines() {
        return land_lines;
    }

    public void setLand_lines(double land_lines) {
        this.land_lines = land_lines;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tariff)) return false;
        Tariff tariff = (Tariff) o;
        return getPayroll() == tariff.getPayroll() &&
                Double.compare(tariff.getSmsWithinUkraine(), getSmsWithinUkraine()) == 0 &&
                Double.compare(tariff.getInternet(), getInternet()) == 0 &&
                Double.compare(tariff.getLand_lines(), getLand_lines()) == 0 &&
                getName().equals(tariff.getName()) &&
                getCallprices().equals(tariff.getCallprices());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getPayroll(), getCallprices(), getSmsWithinUkraine(), getInternet(), getLand_lines());
    }

    @Override
    public String toString() {
        return "Tarrif{" +
                "name='" + name + '\'' +
                ", payroll=" + payroll +
                ", callprices=" + callprices +
                ", SmsWithinUkraine=" + SmsWithinUkraine +
                ", internet=" + internet +
                ", land_lines=" + land_lines +
                '}';
    }
}

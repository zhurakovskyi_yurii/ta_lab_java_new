package com.epam.tarrifs.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Objects;

public class Callprices {
    private double on_net;
    private double to_other_mobile;

    public Callprices() {
    }

    public Callprices(double on_net, double to_other_mobile) {
        this.on_net = on_net;
        this.to_other_mobile = to_other_mobile;
    }

    public double getOn_net() {
        return on_net;
    }

    public void setOn_net(double on_net) {
        this.on_net = on_net;
    }

    public double getTo_other_mobile() {
        return to_other_mobile;
    }

    public void setTo_other_mobile(double to_other_mobile) {
        this.to_other_mobile = to_other_mobile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Callprices)) return false;
        Callprices that = (Callprices) o;
        return Double.compare(that.getOn_net(), getOn_net()) == 0 &&
                Double.compare(that.getTo_other_mobile(), getTo_other_mobile()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOn_net(), getTo_other_mobile());
    }

    @Override
    public String toString() {
        return "Callprices{" +
                "on_net=" + on_net +
                ", to_other_mobile=" + to_other_mobile +
                '}';
    }
}

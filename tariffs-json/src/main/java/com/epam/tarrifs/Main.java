package com.epam.tarrifs;


import com.epam.tarrifs.readfromjson.WorksWithJSONFile;

import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) {
        try {
            WorksWithJSONFile.readTarrifsFromFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}

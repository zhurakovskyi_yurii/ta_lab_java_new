package com.epam.hw.string.container;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        StringContainer stringContainer = new StringContainer();
        for (int i = 0; i < 10; i++) {
            stringContainer.addString("String" + i);
        }
        logger.info("\nStrings from StringContainer:\n");
        for (int i = 0; i < 10; i++) {
            logger.info(stringContainer.getString(i) + "\n");
        }
        List<String> stringList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            stringList.add("String" + i);
        }
        logger.info("\n\nStrings from stringList:\n");
        for (int i = 0; i < 10; i++) {
            logger.info(stringList.get(i) + "\n");
        }
        timeComparisonBetweenArrayListAndStringContainer(stringContainer, stringList);
    }

    public static void timeComparisonBetweenArrayListAndStringContainer(StringContainer stringContainer, List<String> stringList) {
        logger.info("\n Time for adding into my StringContainer is " + testTimeStringContainer(stringContainer) + "\n");
        logger.info("\n Time for adding into ArrayList of strings is " + testArrayListWithStrings(stringList) + "\n");
    }

    public static long testTimeStringContainer(StringContainer stringContainer) {
        long start = System.nanoTime();
        for (int i = 0; i < 300000; i++) {
            stringContainer.addString("String " + i + "of the String Container");
        }
        return System.nanoTime() - start;
    }

    public static long testArrayListWithStrings(List<String> stringList) {
        long start = System.nanoTime();
        for (int i = 0; i < 300000; i++) {
            stringList.add("String " + i + "of the String Container");
        }
        return System.nanoTime() - start;
    }
}

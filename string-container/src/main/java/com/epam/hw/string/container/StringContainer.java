package com.epam.hw.string.container;

import java.util.Arrays;
import java.util.NoSuchElementException;

public class StringContainer {
    private int size;
    private int pointer;
    private String[] strings;

    public StringContainer() {
        this.size = 0;
        this.strings = new String[5];
        pointer = -1;
    }

    public void addString(String string){
        if (pointer + 1 > strings.length - 1){
            increaseContainer();
        }
        pointer++;
        strings[pointer] = string;
        size++;
    }

    public void increaseContainer(){
        strings = Arrays.copyOf(strings, strings.length + 5);
    }

    public String getString(int index){
        if(size <= index)
            throw new NoSuchElementException();
        return strings[index];
    }

    public int getSize() {
        return size;
    }
}

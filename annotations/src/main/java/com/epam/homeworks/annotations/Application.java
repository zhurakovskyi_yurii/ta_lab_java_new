package com.epam.homeworks.annotations;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Field;

public class Application {

    private static final Logger logger = LogManager.getLogger(Application.class);
    private static Product product = new Product(22, "water", 22);

    public static void main(String[] args) {
        printProduct(product);
    }

    public static void printProduct(Product product) {
        try {
            Field[] fields = product.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Field field = fields[i];
                field.setAccessible(true);

                logger.info(fields[i].getAnnotation(PrintProduct.class).value() + " : " + fields[i].get(product) + "\n");
            }
        }
        catch(IllegalAccessException e){
            e.printStackTrace();
        }
    }
}
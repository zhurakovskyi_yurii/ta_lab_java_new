package com.epamtask.dequeue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.Iterator;

public interface IMyDequeue<T> {
    boolean add(T elem);
    void addFirst(T elem);
    void addLast(T elem);
    Iterator<T> iterator();
    boolean offer(T elem);
    void clear();
    boolean isEmpty();
    T pollFirst();
    T pollLast();
    T peekFirst();
    T peekLast();
}
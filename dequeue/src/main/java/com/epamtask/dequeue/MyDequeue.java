package com.epamtask.dequeue;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyDequeue<T> implements IMyDequeue<T> {
    private transient Node<T> firstNode;
    private transient Node<T> lastNode;
    private int size = 0;

    public MyDequeue() {
    }

    protected int size() {
        return size;
    }

    @Override
    public boolean add(T elem) {
        addLast(elem);
        return true;
    }

    @Override
    public void addFirst(T elem) {
        Node<T> newNode = new Node<T>(elem);
        if (firstNode != null) {
            firstNode.prevNode = newNode;
            newNode.nextNode = firstNode;
            firstNode = newNode;
        } else {
            firstNode = lastNode = newNode;
        }
        ++size;
    }

    @Override
    public void addLast(T elem) {
        Node<T> newNode = new Node<T>(elem);
        if (lastNode != null) {
            lastNode.nextNode = newNode;
            newNode.prevNode = lastNode;
            lastNode = newNode;
        } else {
            firstNode = lastNode = newNode;
        }
        ++size;
    }

    @Override
    public Iterator<T> iterator() {
        return new MyDequeueIterator<>();
    }

    @Override
    public boolean offer(T elem) {
        add(elem);
        return true;
    }

    @Override
    public void clear() {
        while (firstNode.nextNode != null) {
            firstNode = firstNode.nextNode;
            firstNode.prevNode = null;
            firstNode.nextNode = null;
        }
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public T pollFirst() {
        return firstNode == null ? null : removeFirst();
    }

    public T peekFirst(){
        return isEmpty()? null : (T)firstNode;
    }

    public T peekLast(){
        return isEmpty()? null : (T)lastNode;
    }

    private T removeFirst() {
        Node<T> node = firstNode;
        firstNode.nextNode = firstNode.nextNode.nextNode;
        firstNode = firstNode.nextNode;
        size--;
        return (T) node;
    }

    public T pollLast() {
        return lastNode == null ? null : removeLast();
    }

    private T removeLast() {
        Node<T> node = lastNode;
        lastNode.prevNode = lastNode.prevNode.prevNode;
        lastNode = lastNode.prevNode;
        size--;
        return (T) node;
    }

    public T remove(int index) {
        if (index < 0 && index >= size)
            new IndexOutOfBoundsException("This index doesn't exist");
        Node<T> node = firstNode;
        Node<T> n = null;
        int counter = 0;
        while (node.nextNode != null) {
            if (counter != index) {
                node = node.nextNode;
                counter++;
            } else {
                node.prevNode.nextNode = node.nextNode;
                node.nextNode.prevNode = node.prevNode;
                size--;
                return (T) node;
            }
        }
        return null;
    }

    public T get(int index) {
        if (index < 0 && index >= size)
            new IndexOutOfBoundsException("This index doesn't exist");
        Node<T> node1 = firstNode;
        int counter1 = 0;
        while (node1.nextNode != null) {
            if (counter1 != index) {
                node1 = node1.nextNode;
                counter1++;
            } else {
                break;
            }
        }
        return (T) node1;
    }

    final class MyDequeueIterator<T> implements Iterator<T> {
        Node<T> thisElem = (Node<T>) firstNode;
        private int pointer = 0;

        @Override
        public boolean hasNext() {
            return thisElem.nextNode != null;
        }

        @Override
        public T next() {
            if (!hasNext())
                new NoSuchElementException();
            ++pointer;
            Node<T> elem = thisElem;
            thisElem = thisElem.nextNode;
            return (T) elem;
        }
    }

    private class Node<T> {
        T current;
        Node<T> prevNode;
        Node<T> nextNode;

        public Node(T current) {
            this.current = current;
        }

        @Override
        public String toString() {
            return current.toString();
        }
    }
}

package com.epamtask.dequeue;

import com.epam.lab.ship.with.droids.array.Droid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        MyDequeue<Droid> myDequeue = fillMyDequeueWithDroidUsingAddLast();
        myDequeue.add(new Droid("first droid"));
        myDequeue.add(new Droid("first droid"));
        myDequeue.add(new Droid("first droid"));
        printAllMyDequeueValuesUsingGet(myDequeue);
        logger.info("\n Try to remove " + myDequeue.remove(9));
        printAllMyDequeueValuesUsingIterator(myDequeue);
        myDequeue = fillMyDequeueWithDroidUsingAddFirst();
        printAllMyDequeueValuesUsingIterator(myDequeue);
        printAllMyDequeueValuesUsingIterator(myDequeue);
        logger.info("\n\n" + myDequeue.remove(6));
        printAllMyDequeueValuesUsingIterator(myDequeue);
        logger.info("Poll first: " + myDequeue.pollFirst());
        printAllMyDequeueValuesUsingIterator(myDequeue);
        logger.info("Poll last: " + myDequeue.pollLast());
        printAllMyDequeueValuesUsingIterator(myDequeue);
    }

    public static MyDequeue<Droid> fillMyDequeueWithDroidUsingAddLast() {
        logger.info("\nUse Add Last\n");
        MyDequeue<Droid> myDequeue = new MyDequeue<>();
        for (int i = 0; i < 10; i++) {
            myDequeue.add(new Droid((i + 1) + " droid"));
        }
        return myDequeue;
    }

    public static MyDequeue<Droid> fillMyDequeueWithDroidUsingAddFirst() {
        logger.info("\nUse Add First\n");
        MyDequeue<Droid> myDequeue = new MyDequeue<>();
        for (int i = 0; i < 10; i++) {
            myDequeue.addFirst(new Droid((i + 1) + " droid"));
        }
        return myDequeue;
    }

    public static void printAllMyDequeueValuesUsingGet(MyDequeue<Droid> myDequeue) {
        logger.info("\n(Using Get) All elements: \n\n");
        for (int i = 0; i < myDequeue.size(); i++) {
            logger.info(myDequeue.get(i) + "\n");
        }
    }

    public static void printAllMyDequeueValuesUsingIterator(MyDequeue<Droid> myDequeue) {
        Iterator iterator = myDequeue.iterator();
        logger.info("\n(Using Iterator) All elements: \n\n");
        while (iterator.hasNext()) {
            logger.info(iterator.next() + "\n");
        }
    }
}

package com.epam.lab.ship.with.droids.array;

import java.io.Serializable;
import java.util.Objects;

public class Droid implements Comparable<Droid>, Serializable {
    private String name;

    public Droid(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Droid)) return false;
        Droid droid = (Droid) o;
        return getName().equals(droid.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    @Override
    public int compareTo(Droid o) {
        return name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                '}';
    }
}

package com.epam.flowershop;

public class Bouquet {
    private Events event;
    private double cost;
    private String additional;
    private String name = "Bouquet";

    public Bouquet(){
    }

    public Bouquet(Events event,double cost){
        this.event = event;
        this.cost = cost;
        name += (" for " + event.name());
    }

    public Events getEvent() {
        return event;
    }

    public void setEvent(Events event) {
        this.event = event;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "event=" + event +
                ", cost=" + cost +
                '}';
    }

    protected void setAdditional(String additional) {
        this.additional = additional;
    }

    public String getName(){
        return name;
    }
}

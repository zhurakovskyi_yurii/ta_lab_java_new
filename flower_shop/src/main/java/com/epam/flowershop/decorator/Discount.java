package com.epam.flowershop.decorator;

public class Discount extends BouquetDecorator {
    private static final int COST = -20;

    public Discount() {
        setAdditional(" + Discount");
        setAdditionalPrice(COST);
    }

    public static int getCOST() {
        return COST;
    }
}

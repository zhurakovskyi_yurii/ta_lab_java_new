package com.epam.flowershop.decorator;

public class Delivery extends BouquetDecorator {
    private static final int COST = 35;

    public Delivery() {
        setAdditional(" + Delivery");
        setAdditionalPrice(COST);
    }

    public static int getCOST() {
        return COST;
    }


}

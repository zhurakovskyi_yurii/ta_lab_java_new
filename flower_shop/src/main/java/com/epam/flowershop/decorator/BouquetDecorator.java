package com.epam.flowershop.decorator;

import com.epam.flowershop.Bouquet;

import java.util.Optional;

public class BouquetDecorator extends Bouquet {
    Optional<Bouquet> bouquet;
    private double additionalPrice;
    private String toName = "";

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
    }

    public String getName(){
        return bouquet.orElseThrow(IllegalAccessError::new).getName() + toName;
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    public double getPrice() {
        return bouquet.orElseThrow(IllegalArgumentException::new).getCost() + additionalPrice;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }

}

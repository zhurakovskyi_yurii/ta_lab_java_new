package com.epam.flowershop;

import com.epam.flowershop.decorator.BouquetDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        FlowerShop flowerShop = new FlowerShop();
        BouquetDecorator bouquetDecorator = flowerShop.propose(Events.BIRTHDAY);
        logger.info(bouquetDecorator.getName() + " " + bouquetDecorator.getPrice());
    }
}

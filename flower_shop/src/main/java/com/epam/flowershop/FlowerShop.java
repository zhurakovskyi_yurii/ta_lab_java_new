package com.epam.flowershop;

import com.epam.flowershop.decorator.BouquetDecorator;
import com.epam.flowershop.decorator.Discount;
import com.epam.flowershop.packaging.PaperPackaging;
import com.epam.flowershop.packaging.PlasticPackaging;

import static com.epam.flowershop.Events.*;

public class FlowerShop {
    public BouquetDecorator propose(Events event){
        BouquetDecorator bouquetDecorator = null;
        switch (event) {
            case WEDDING:
                Bouquet bouquet1 = new Bouquet(WEDDING, 580);
                bouquetDecorator = new PaperPackaging();
                bouquetDecorator.setBouquet(bouquet1);
                break;
            case FUNERAL:
                Bouquet bouquet2 = new Bouquet(FUNERAL, 350);
                bouquetDecorator = new PlasticPackaging();
                bouquetDecorator.setBouquet(bouquet2);
                break;
            case BIRTHDAY:
                Bouquet bouquet3 = new Bouquet(BIRTHDAY, 240);
                bouquetDecorator = new Discount();
                bouquetDecorator.setBouquet(bouquet3);
                break;
            case VALENTINES_DAY:
                Bouquet bouquet4 = new Bouquet(VALENTINES_DAY, 340);
                bouquetDecorator = new Discount();
                bouquetDecorator.setBouquet(bouquet4);
                break;
        }
        return bouquetDecorator;
    }
}

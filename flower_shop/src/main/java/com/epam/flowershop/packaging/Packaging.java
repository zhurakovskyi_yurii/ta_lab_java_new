package com.epam.flowershop.packaging;

import com.epam.flowershop.decorator.BouquetDecorator;

public class Packaging extends BouquetDecorator {
    private static final int COST = 20;

    public Packaging() {
        setAdditional(" + Packaging");
        setAdditionalPrice(COST);
    }

    public static int getCOST() {
        return COST;
    }
}

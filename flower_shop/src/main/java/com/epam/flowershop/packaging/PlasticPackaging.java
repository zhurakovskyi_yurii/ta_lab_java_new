package com.epam.flowershop.packaging;

public class PlasticPackaging extends Packaging {
    private static final int COST = 34;

    public PlasticPackaging(){
        setAdditional(" + Plastic Packaging");
        setAdditionalPrice(COST);
    }
}

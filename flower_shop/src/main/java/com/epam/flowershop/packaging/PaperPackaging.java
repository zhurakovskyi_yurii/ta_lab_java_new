package com.epam.flowershop.packaging;


public class PaperPackaging extends Packaging {
    private static final int COST = 54;

    public PaperPackaging() {
        setAdditional(" + Paper Packaging");
        setAdditionalPrice(COST);
    }
}

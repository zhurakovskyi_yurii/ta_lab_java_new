package my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;
import com.epam.lab.ship.with.droids.array.IG_88;
import com.epam.lab.ship.with.droids.array.R2_D2;
import com.epam.lab.ship.with.droids.array.TC;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Iterator;
import java.util.PriorityQueue;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        IMyPriorityQueue<Droid> droids = new MyPriorityQueue<>();
        droids.add(new IG_88());
        droids.add(new R2_D2());
        droids.add(new TC());
        for (int i = 0; i < 5; i++) {
            droids.add(new Droid("droid" + i));
        }
        printAllDroids(droids);
        logger.info(droids.poll());
        printAllDroids(droids);
        logger.info("\n\n" + droids.peek());
    }

    public static void printAllDroids(IMyPriorityQueue<Droid> droids){
        Iterator iterator = droids.iterator();
        logger.info("\nAll droids: \n\n");
        while (iterator.hasNext()){
            logger.info(iterator.next() + "\n");
        }
    }
}

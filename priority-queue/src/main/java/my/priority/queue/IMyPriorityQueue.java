package my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.Iterator;

public interface IMyPriorityQueue<T> {
    void add(T droid);
    Object[] toArray();
    T poll();
    T peek();
    Iterator<T> iterator();
    void clear();
    boolean isEmpty();
}

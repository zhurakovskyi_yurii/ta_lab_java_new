package my.priority.queue;

import com.epam.lab.ship.with.droids.array.Droid;

import java.util.*;

public class MyPriorityQueue<T> implements IMyPriorityQueue {
    private int size;
    private transient Object[] objects;
    private Comparator<T> comparator;

    public int size() {
        return size;
    }

    @Override
    public void add(Object object) {
        if (objects == null)
            objects = new Droid[1];
        else {
            objects = Arrays.copyOf(objects, objects.length + 1);
        }
        objects[objects.length - 1] = object;
        size = objects.length;
    }

    public void remove(Object object) {
        int index = findIndexOfElement(object);
        Object[] objects1 = Arrays.copyOfRange(objects, 0, index);
        Object[] objects2 = Arrays.copyOfRange(objects, index + 1, objects.length - 1);
        Object[] res = new Object[objects1.length + objects2.length];
        System.arraycopy(objects1, 0, res, 0, objects1.length);
        System.arraycopy(objects2, 0, res, index, objects2.length);
        size--;
        objects = res;
    }

    private int findIndexOfElement(Object object) {
        for (int i = 0; i < objects.length; i++) {
            if (objects[i].equals(object))
                return i;
        }
        return -1;
    }

    public Object[] toArray() {
        return Arrays.copyOf(objects, size);
    }


    public T poll() {
        if (objects != null) {
            T droid = (T) objects[0];
            for (int i = 0; i < objects.length - 1; i++) {
                objects[i] = objects[i + 1];
            }
            objects = Arrays.copyOf(objects, objects.length - 1);
            size = objects.length;
            return droid;
        }
        return null;
    }


    public T peek() {
        if (objects != null)
            return (T) objects[0];
        return null;
    }

    @Override
    public Iterator iterator() {
        return new MyPriorityQueueIterator();
    }

    public void clear() {
        size = 0;
        objects = null;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private final class MyPriorityQueueIterator<T> implements Iterator<T> {
        private int pointer = 0;

        public boolean hasNext() {
            return pointer < size;
        }

        public T next() {
            return (T) objects[pointer++];
        }
    }
}

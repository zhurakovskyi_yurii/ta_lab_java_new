package com.epam.homeworks.lambdas.streams.second.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class House implements IHouseCommand {
    private String address;
    private HouseType houseType;
    private static final Logger logger = LogManager.getLogger(House.class);

    public House(String address, HouseType houseType) {
        this.address = address;
        this.houseType = houseType;
    }

    public void turningOnAnAlarmSystem(RoomOrPlace roomOrPlace) {
        logger.info(roomOrPlace + " Turning on an alarm system");
    }

    public void cookingSomething(String dish) {
        logger.info("Cooking " + dish);
    }

    public void cleaningRoom(RoomOrPlace roomOrPlace) {
        logger.info("Cleaning " + roomOrPlace.name().toLowerCase());
    }

    public void parkingCar() {
        logger.info("Parking a car");
    }

    @Override
    public String toString() {
        return "House{" +
                "address='" + address + '\'' +
                ", houseType=" + houseType +
                '}';
    }
}

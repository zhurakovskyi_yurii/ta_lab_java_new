package com.epam.homeworks.lambdas.streams.fourth.task;

public class Main {
    public static void main(String[] args) {
        TextInspector textInspector = new TextInspector();
        textInspector.run();
    }
}

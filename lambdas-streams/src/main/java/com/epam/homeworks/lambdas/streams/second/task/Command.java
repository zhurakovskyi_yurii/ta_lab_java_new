package com.epam.homeworks.lambdas.streams.second.task;

public interface Command {
    void execute();
}

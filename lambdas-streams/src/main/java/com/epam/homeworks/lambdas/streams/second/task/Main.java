package com.epam.homeworks.lambdas.streams.second.task;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        House house = new House("4177  Parker Drive", HouseType.SINGLE_FAMILY_DETACHED_HOUSE);
        CleaningRoom cleaningRoom = new CleaningRoom(house, RoomOrPlace.FAMILY_ROOM);
        Command command2 = new Command() {
            @Override
            public void execute() {
                house.turningOnAnAlarmSystem(RoomOrPlace.GARAGE);
            }
        };
        Command command3 = () -> house.cookingSomething("some dish");
        Command command4 = house::parkingCar;
        Robot robot = new Robot();
        boolean condition = true;
        menu();
        String command = "";
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));) {
            command = bufferedReader.readLine().toLowerCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (command) {
            case "parking":
                robot.addCommand(command4);
                break;
            case "cooking":
                robot.addCommand(command3);
                break;
            case "alarm":
                robot.addCommand(command2);
                break;
            case "cleaning":
                robot.addCommand(cleaningRoom);
                break;
            default:
                logger.info("\nincorrect command\n");
                break;
        }
        robot.run();
    }

    public static void menu() {
        logger.info("\nCommands:");
        logger.info("\nParking");
        logger.info("\nCooking");
        logger.info("\nCleaning");
        logger.info("\n(Turning on an alarm system) Alarm\n");
    }
}

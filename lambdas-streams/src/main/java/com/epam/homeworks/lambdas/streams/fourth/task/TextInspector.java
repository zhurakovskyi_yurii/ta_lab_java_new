package com.epam.homeworks.lambdas.streams.fourth.task;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class TextInspector {
    private List<String> stringList;
    private static final Logger logger = LogManager.getLogger(TextInspector.class);

    public TextInspector() {
        stringList = new ArrayList<>();
    }

    public void run() {
        logger.info("Enter some strings!\n");
        inputText();
        print(stringList);
        logger.info("\nNumber of unique words = " + numberOfUniqueWords());
        logger.info("\nSorted list of unique words: \n");
        print(getSortedListOfUniqueWords());
        logger.info("\nList with occurrence numbers of words: \n");
        print(getListWithOccurrenceNumbersOfWords(stringList));
        logger.info("\nList with occurrence numbers of each symbol except upper case characters: \n");
        print(getListWithOccurrenceNumbersOfSymbolsExceptUpperCaseCharacters(stringList));
    }

    private void inputText() {
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                String string = bufferedReader.readLine();
                string = string.trim();
                if (string.length() > 0)
                    stringList.addAll(Arrays.stream(string.split(" ")).collect(Collectors.toList()));
                else
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int numberOfUniqueWords() {
        return (int) getUniqueWords().stream().count();
    }

    private List<String> getUniqueWords() {
        return stringList.stream().collect(Collectors.toList());
    }

    private List<String> getSortedListOfUniqueWords() {
        return getUniqueWords().stream().sorted().collect(Collectors.toList());
    }

    private void print(List<String> strings) {
        strings.stream().forEach(s -> logger.info(s + " "));
    }

    private List<String> getListWithOccurrenceNumbersOfWords(List<String> list) {
        return list.stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting())).entrySet().stream().map(e -> e.getKey() + "-" + e.getValue()).collect(Collectors.toList());
    }

    private List<String> getListWithOccurrenceNumbersOfSymbolsExceptUpperCaseCharacters(List<String> list) {
        String symbols = list.stream().distinct().collect(Collectors.joining(""));
        List<String> stringList = symbols.chars().filter(c -> !Character.isUpperCase(c)).mapToObj(c -> (char) c + "").collect(Collectors.toList());
        return getListWithOccurrenceNumbersOfWords(stringList);
    }
}

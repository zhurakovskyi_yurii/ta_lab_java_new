package com.epam.homeworks.lambdas.streams.first.task;

@FunctionalInterface
public interface IThreeParametersFunction {
    int accept(int value1, int value2, int value3);
}

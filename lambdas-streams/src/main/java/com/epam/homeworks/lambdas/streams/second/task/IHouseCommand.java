package com.epam.homeworks.lambdas.streams.second.task;

public interface IHouseCommand {
    void turningOnAnAlarmSystem(RoomOrPlace roomOrPlace);

    void cookingSomething(String dish);

    void cleaningRoom(RoomOrPlace roomOrPlace);

    void parkingCar();
}

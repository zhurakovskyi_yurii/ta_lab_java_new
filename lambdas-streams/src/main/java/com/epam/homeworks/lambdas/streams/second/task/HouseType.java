package com.epam.homeworks.lambdas.streams.second.task;

public enum HouseType {
    SINGLE_FAMILY_DETACHED_HOUSE,
    APARTMENT,
    BUNGALOW,
    CABIN,
    CARRIAGE_COACH_HOUSE,
    CASTLE,
    CAVE_HOUSE,
    CHALET
}

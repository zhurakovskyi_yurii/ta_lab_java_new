package com.epam.homeworks.tariffs.project.controller;

import com.epam.homeworks.tariffs.project.controller.tariff.factory.TariffFactory;
import com.epam.homeworks.tariffs.project.model.*;
import com.epam.homeworks.tariffs.project.services.*;
import com.epam.homeworks.tariffs.project.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);
    public static Scanner scanner = new Scanner(System.in);

    public static void registration() {
        User user = new User();
        user.setAccount(new AccountEntity());
        while (true) {
            logger.info("Введіть номер телефону у форматі +380955030740: \n");
            String phone = scanner.nextLine();
            if (Controller.checkPhone(phone)) {
                logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                continue;
            } else user.getAccount().setPhone_number(phone);
            logger.info("Введіть ім'я\n");
            String firstName = scanner.nextLine().trim();
            user.setFirst_name(firstName);
            logger.info("Ім'я: " + firstName + "\n");
            logger.info("Введіть прізвище\n");
            String lastName = scanner.nextLine().trim();
            user.setLast_name(lastName);
            logger.info("Ім'я та прізвище: " + firstName + " " + lastName + "\n");
            logger.info("\nВиберіть тарифний план\n1 - Speaking, 2 - SmartphoneZero, 3 - Non_Limit_Internet\n");
            String choice = scanner.nextLine().trim();
            Tariff tariff = TariffFactory.chooseTariff(choice);
            user.setTariff(tariff);
            logger.info(firstName + " " + lastName + ", вибрано тариф " + user.getTariff().getName() + "\n\n");
            user.getAccount().setBalance(100);
            user.getAccount().setLast_replenishment_date(new Date(new
                    java.util.Date().getTime()));
            user.getAccount().setActive_to(new Date(new
                    java.util.Date().getTime()));
            user.getAccount().setId(1);
            AccountService accountService = new AccountService();
            accountService.create(user.getAccount());
            UserService userService = new UserService();
            userService.create(user);
            break;
        }
    }

    public static void showAllUsers() {
        List<User> users = new UserService().findAll();
        logger.info("Всі зареєстровані номери : \n");
        for (User user : users) {
            logger.info(user.getFirst_name() + " " + user.getLast_name() + " " + user.getAccount().getPhone_number() + "\n");
        }
        logger.info("\n");
    }

    public static void changeTariff() {
        User user = new User();
        user.setAccount(new AccountEntity());
        logger.info("Введіть номер телефону у форматі +380955030740: \n");
        String phone = scanner.nextLine();
        if (Controller.checkPhone(phone)) {
            logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
        } else {
            if (new UserService().findByPhone(phone).isPresent()) {
                user = new UserService().findByPhone(phone).get();
                logger.info("\nВиберіть тарифний план\n1 - Speaking, 2 - SmartphoneZero, 3 - Non_Limit_Internet\n");
                String choice = Controller.scanner.nextLine().trim();
                Tariff tariff = TariffFactory.chooseTariff(choice);
                String name = user.getTariff().getName();
                user.setTariff(tariff);
                UserService userService = new UserService();
                userService.update(user);
                logger.info("\n Ви змінили тариф з " + name + " на: "
                        + userService.findByPhone(user.getAccount().getPhone_number()).orElseThrow(IllegalArgumentException::new).getTariff().getName() + "\n");
            }
        }
    }

    public static void callOtherUser() {
        User user = new User();
        user.setAccount(new AccountEntity());
        logger.info("Введіть номер телефону у форматі +380955030740: \n");
        String phone = scanner.nextLine();
        if (Controller.checkPhone(phone)) {
            logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
        } else {
            try {
                user = new UserService().findByPhone(phone).orElseThrow(IllegalArgumentException::new);
                if (user.getAccount().getBalance() > user.getTariff().getCalls_on_net_cost()) {
                    double oldBalanceValue = user.getAccount().getBalance();
                    double newBalanceValue = user.getAccount().getBalance() - user.getTariff().getCalls_on_net_cost();
                    CallEntity callEntity = new CallEntity();
                    callEntity.setCuser_from(user.getId());
                    callEntity.setDate(new Date(new java.util.Date().getTime()));
                    showAllUsers();
                    logger.info("Введіть зареєстрований номер отримувача: \n");
                    String phoneR = Controller.scanner.nextLine();
                    if (phoneR.length() != 13 || phoneR.charAt(0) != '+' || !isDigit(phoneR.substring(1))) {
                        logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                    } else {
                        try {
                            User user2 = new UserService().findByPhone(phoneR).orElseThrow(IllegalArgumentException::new);
                            callEntity.setCuser_to(user2.getId());
                            user.getAccount().setBalance(newBalanceValue);
                            logger.info("Старий баланс: " + oldBalanceValue + " Новий баланс: " + newBalanceValue + "\n");
                            AccountService accountService = new AccountService();
                            accountService.update(user.getAccount());
                            logger.info("Виклик здійснено\n");
                        } catch (IllegalArgumentException e) {
                            logger.info("Такого телефону в базі немає\n");
                        }
                    }
                    new CallService().create(callEntity);
                }
            }catch (IllegalArgumentException e){
                logger.info("Такого телефону в базі немає\n");
            }
        }
    }

    public static void sendSMS() {
        logger.info("Введіть номер відправника \n");
        String phone = scanner.nextLine();
        if (checkPhone(phone)) {
            logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
        }
        try {
            User user = new UserService().findByPhone(phone).orElseThrow(IllegalArgumentException::new);
            if (user.getAccount().getBalance() > user.getTariff().getSms_within_ukraine_cost()) {
                logger.info("Ваше повідомлення: \n");
                String text = scanner.nextLine();
                SMS sms = new SMS();
                sms.setText(text);
                sms.setDate(new Date(new java.util.Date().getTime()));
                showAllUsers();
                logger.info("\nВведіть зареєстрований номер отримувача: \n");
                String phoneR = Controller.scanner.nextLine();
                if (phoneR.length() != 13 || phoneR.charAt(0) != '+' || !isDigit(phoneR.substring(1))) {
                    logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
                } else {
                    try {
                        User user2 = new UserService().findByPhone(phoneR).orElseThrow(IllegalArgumentException::new);
                        sms.setUser_to(user2.getId());
                        sms.setUser_from(user.getId());
                        double oldBalanceValue = user.getAccount().getBalance();
                        double newBalanceValue = user.getAccount().getBalance() - user.getTariff().getSms_within_ukraine_cost();
                        user.getAccount().setBalance(user.getAccount().getBalance() - user.getTariff().getSms_within_ukraine_cost());
                        new AccountService().update(user.getAccount());
                        logger.info("Старий баланс: " + oldBalanceValue + " Новий баланс: " + newBalanceValue + "\n");
                        new SMSService().create(sms);
                        logger.info("Відправлено\n");
                    } catch (IllegalArgumentException e) {
                        logger.info("\nТакого телефону в базі немає\n");
                    }
                }
            }
        } catch(IllegalArgumentException e){
            logger.info("\n Такого телефону в базі немає\n");
        }
    }

    public static boolean isDigit(String substring) {
        for (char c : substring.toCharArray()) {
            if (!Character.isDigit(c))
                return false;
        }
        return true;
    }

    public static void showView() {
        View view = new View();
        view.view();
        logger.info("\n\n");
        for (Map.Entry<String, String> entry : View.menu.entrySet()) {
            logger.info(entry.getKey() + " " + entry.getValue() + "\n");
        }
    }

    public static void start() {
        boolean exit = false;
        while (!exit) {
            showView();
            String choice = scanner.nextLine().trim().toUpperCase();
            switch (choice) {
                case "1":
                    registration();
                    break;
                case "2":
                    changeTariff();
                    break;
                case "3":
                    delete();
                    break;
                case "4":
                    replenishment();
                    break;
                case "5":
                    callOtherUser();
                    break;
                case "6":
                    sendSMS();
                    break;
                case "7":
                    showAllUsers();
                    break;
                case "EXIT":
                    exit = true;
                    break;
                default:
                    logger.info("\nНевірний вибір\n");
            }
        }
    }

    private static void replenishment() {
        logger.info("Введіть номер телефону у форматі +380955030740: \n");
        String phone = scanner.nextLine();
        if (checkPhone(phone)) {
            logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
        } else {
            AccountService accountService = new AccountService();
            AccountEntity accountEntity = accountService.findByPhone(phone).orElseThrow(IllegalArgumentException::new);
            logger.info("Введіть суму поповнення 10, 25, 50, 100\n");
            String choice = scanner.nextLine().trim();
            switch (choice) {
                case "10":
                    accountEntity.setBalance(accountEntity.getBalance() + 10);
                    accountService.update(accountEntity);
                    logger.info("Рахунок поповнено на 10 грн і тепер становить : ");
                    logger.info(accountService.findByPhone(phone).get().getBalance() + " грн\n");
                    break;
                case "25":
                    accountEntity.setBalance(accountEntity.getBalance() + 25);
                    accountService.update(accountEntity);
                    logger.info("Рахунок поповнено на 25 грн і тепер становить : ");
                    logger.info(accountService.findByPhone(phone).get().getBalance() + " грн\n");
                    break;
                case "50":
                    accountEntity.setBalance(accountEntity.getBalance() + 50);
                    accountService.update(accountEntity);
                    logger.info("Рахунок поповнено на 50 грн і тепер становить : \n");
                    logger.info(accountService.findByPhone(phone).get().getBalance() + " грн\n");
                    break;
                case "100":
                    accountEntity.setBalance(accountEntity.getBalance() + 100);
                    accountService.update(accountEntity);
                    logger.info("Рахунок поповнено на 100 грн і тепер становить : \n");
                    logger.info(accountService.findByPhone(phone).get().getBalance() + " грн\n");
                    break;
            }
        }
    }

    public static void delete() {
        logger.info("Введіть номер телефону у форматі +380955030740: \n");
        String phone = scanner.nextLine();
        if (checkPhone(phone)) {
            logger.info("Невірний формат номеру, введіть у форматі +380955030740\n");
        } else {
            new UserService().deleteByPhone(phone);
            if (!new UserService().findByPhone(phone).isPresent())
                logger.info("Користувача видалено!");
        }
    }

    public static boolean checkPhone(String phone) {
        return phone.length() != 13 || phone.charAt(0) != '+' || !isDigit(phone.substring(1));
    }
}

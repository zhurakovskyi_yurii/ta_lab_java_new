package com.epam.homeworks.tariffs.project.persistant;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.stream.Collectors;

public class ConnectionManager implements AutoCloseable {

    private static String URL = "";
    private static String USER_NAME = "";
    private static String USER_PASSWORD = "";
    private static String DRIVER = "";

    private static Connection connection = null;

    static {
        try {
            if(connection == null)
                connection = ConnectionManager.openConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection(){
        return connection;
    }
    private ConnectionManager() {
    }

    private static Connection openConnection() throws SQLException {
        try (InputStream inputStream = new ConnectionManager().getClass().getResourceAsStream("/db.properties");
             BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {
            String contents = bufferedReader.lines()
                    .collect(Collectors.joining(System.lineSeparator()));
            String[] contentStrs = contents.split("\n");
            DRIVER = contentStrs[0].split("=")[1].trim();
            URL = contentStrs[1].substring(7).trim();
            USER_NAME = contentStrs[2].split("=")[1].trim();
            USER_PASSWORD = contentStrs[3].split("=")[1].trim();
            Class.forName(DRIVER).newInstance();

        } catch (IOException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        return DriverManager.getConnection(URL, USER_NAME, USER_PASSWORD);
    }


    public void close(){
        if (connection != null){
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}

package com.epam.homeworks.tariffs.project.model;

public class Speaking extends Tariff {

    public Speaking() {
        this.setId(1);
        this.setName("Speaking");
        this.setPayroll(60);
        this.setCalls_on_net_cost(0);
        this.setSms_within_ukraine_cost(0.50);
        this.setInternet(10);
        this.setLand_lines(2.50);
    }
}

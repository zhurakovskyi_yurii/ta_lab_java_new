package com.epam.homeworks.tariffs.project.view;

import java.util.LinkedHashMap;
import java.util.Map;

public class View {
    public static Map<String, String> menu;

    public void view() {
        menu = new LinkedHashMap<>();

        menu.put("1", "Добавити користувача до бази");
        menu.put("2", "Змінити тарифний план для користувача");
        menu.put("3", "Видалити користувача з бази");
        menu.put("4", "Поповнити рахунок для користувача");
        menu.put("5", "Здійснити дзвінок до користувачa, який є в базі");
        menu.put("6", "Відправити смс користувачу, який є в базі");
        menu.put("7", "Вивести всіх користувачів бази");
        menu.put("EXIT", "Вийти");
    }
}

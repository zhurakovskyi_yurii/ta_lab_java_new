package com.epam.homeworks.tariffs.project.model;

import java.sql.Date;

public class SMS {
    private int id;
    private int user_from;
    private int user_to;
    private String text;
    private Date Date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public java.sql.Date getDate() {
        return Date;
    }

    public void setDate(java.sql.Date date) {
        Date = date;
    }

    public int getUser_from() {
        return user_from;
    }

    public void setUser_from(int user_from) {
        this.user_from = user_from;
    }

    public int getUser_to() {
        return user_to;
    }

    public void setUser_to(int user_to) {
        this.user_to = user_to;
    }

    @Override
    public String toString() {
        return "SMSEntity{" +
                "user_from=" + user_from +
                ", user_to=" + user_to +
                '}';
    }
}

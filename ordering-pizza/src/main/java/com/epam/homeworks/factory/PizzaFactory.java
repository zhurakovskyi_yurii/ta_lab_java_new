package com.epam.homeworks.factory;

import com.epam.homeworks.bakeries.Bakery;
import com.epam.homeworks.pizza.*;
import com.epam.homeworks.scrum.state.Prepared;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;

public class PizzaFactory {
    private static Logger logger = LogManager.getLogger(PizzaFactory.class);

    public Pizza bakePizza(Bakery bakery, BufferedReader bufferedReader) throws IOException {
        Pizza pizza = null;
        int choice = 5;
        switch (bakery) {
            case KYIV:
                choice = menu(bufferedReader);
                if (choice == 1)
                    pizza = new Clam("Clam", "thin", "Marinara", 230, Prepared.getInstance());
                else if (choice == 2)
                    pizza = new Pepperoni("Pepperoni", "thin", "", 200, Prepared.getInstance());
                else if (choice == 3)
                    pizza = new Veggie("Veggie", "thick", "", 190, Prepared.getInstance());
                else if (choice == 4)
                    pizza = new Cheese("Cheese", "thin", "Plum Tomato", 185, Prepared.getInstance());
                break;
            case LVIV:
                choice = menu(bufferedReader);
                if (choice == 1)
                    pizza = new Clam("Clam", "thick", "Plum Tomato", 110, Prepared.getInstance());
                else if (choice == 2)
                    pizza = new Pepperoni("Pepperoni", "thin", "Marinara", 110, Prepared.getInstance());
                else if (choice == 3)
                    pizza = new Veggie("Veggie", "thick", "Marinara", 115, Prepared.getInstance());
                else if (choice == 4)
                    pizza = new Cheese("Cheese", "thin", "Plum Tomato", 165, Prepared.getInstance());
                break;
            case DNIPRO:
                choice = menu(bufferedReader);
                if (choice == 1)
                    pizza = new Clam("Clam", "thick", "Marinara", 260, Prepared.getInstance());
                else if (choice == 2)
                    pizza = new Pepperoni("Pepperoni", "thick", "Plum Tomato", 270, Prepared.getInstance());
                else if (choice == 3)
                    pizza = new Veggie("Veggie", "thick", "Plum Tomato", 234, Prepared.getInstance());
                else if (choice == 4)
                    pizza = new Cheese("Cheese", "thick", "Plum Tomato", 185, Prepared.getInstance());
                break;
        }
        if (pizza != null)
            pizza.update();
        return pizza;
    }

    public int menu(BufferedReader bufferedReader) throws IOException  {
        int choice = 0;
        while (true) {
            try {
                logger.info("\nВиберіть свою піцу:");
                logger.info("\n1 для Clam");
                logger.info("\n2 для Pepperoni");
                logger.info("\n3 для Veggie");
                logger.info("\n4 для Сheese");
                logger.info("\n5 для скасування\n");
                choice = Integer.parseInt(bufferedReader.readLine());
            } catch (NumberFormatException e) {
                logger.info("Невірний ввід");
            }
            if (choice >= 1 && choice <= 5)
                break;
        }
        bufferedReader.close();
        return choice;
    }
}

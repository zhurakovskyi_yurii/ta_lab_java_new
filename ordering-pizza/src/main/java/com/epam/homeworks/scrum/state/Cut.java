package com.epam.homeworks.scrum.state;

import com.epam.homeworks.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cut extends PizzaState {
    private static Logger logger = LogManager.getLogger(Cut.class);
    private static Cut instance = new Cut();
    private Cut() {}
    public static Cut getInstance() {
        return instance;
    }

    @Override
    public void updateState(Pizza pizza) {
        logger.info("The pizza is cut!\n");
        pizza.setPizzaState(Boxed.getInstance());
    }
}

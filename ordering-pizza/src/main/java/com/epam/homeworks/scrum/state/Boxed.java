package com.epam.homeworks.scrum.state;

import com.epam.homeworks.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Boxed extends PizzaState {
    private static Logger logger = LogManager.getLogger(Boxed.class);
    private static Boxed instance = new Boxed();
    private Boxed() {}
    public static Boxed getInstance() {
        return instance;
    }

    @Override
    public void updateState(Pizza pizza) {
        logger.info("The pizza is boxed!\n");
    }
}

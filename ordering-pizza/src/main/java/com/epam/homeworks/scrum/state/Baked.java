package com.epam.homeworks.scrum.state;

import com.epam.homeworks.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Baked extends PizzaState {
    private static Logger logger = LogManager.getLogger(Baked.class);
    private static Baked instance = new Baked();
    private Baked() {}
    public static Baked getInstance() {
        return instance;
    }

    @Override
    public void updateState(Pizza pizza) {
        logger.info("The pizza is baked!\n");
        pizza.setPizzaState(Cut.getInstance());
    }
}

package com.epam.homeworks.scrum.state;

import com.epam.homeworks.pizza.Pizza;

public abstract class PizzaState {
    public abstract void updateState(Pizza pizza);
}

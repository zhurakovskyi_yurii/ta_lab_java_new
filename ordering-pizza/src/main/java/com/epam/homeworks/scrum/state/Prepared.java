package com.epam.homeworks.scrum.state;

import com.epam.homeworks.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Prepared extends PizzaState {
    private static Logger logger = LogManager.getLogger(PizzaState.class);
    private static Prepared instance = new Prepared();
    private Prepared() {}
    public static Prepared getInstance(){
        return instance;
    }
    @Override
    public void updateState(Pizza pizza) {
        logger.info("The pizza is prepared!\n");
        pizza.setPizzaState(Baked.getInstance());
    }
}

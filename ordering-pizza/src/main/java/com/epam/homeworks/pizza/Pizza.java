package com.epam.homeworks.pizza;


import com.epam.homeworks.scrum.state.PizzaState;

public abstract class Pizza {
    private PizzaState pizzaState;
    private String pizzaType;
    private String dough;
    private String sauce;
    private double cost;

    public Pizza(String pizzaType, String dough, String sauce, double cost, PizzaState pizzaState) {
        this.pizzaType = pizzaType;
        this.dough = dough;
        this.sauce = sauce;
        this.pizzaState = pizzaState;
    }

    public PizzaState getPizzaState() {
        return pizzaState;
    }

    public void setPizzaState(PizzaState pizzaState) {
        this.pizzaState = pizzaState;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public double getCost() {
        return cost;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getDough() {
        return dough;
    }

    public void setDough(String dough) {
        this.dough = dough;
    }

    public String getSauce() {
        return sauce;
    }

    public void setSauce(String sauce) {
        this.sauce = sauce;
    }

    public void update(){
        pizzaState.updateState(this);
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "pizzaType='" + pizzaType + '\'' +
                ", dough='" + dough + '\'' +
                ", sauce='" + sauce + '\'' +
                '}';
    }
}

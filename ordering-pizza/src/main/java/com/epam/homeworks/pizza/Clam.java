package com.epam.homeworks.pizza;

import com.epam.homeworks.scrum.state.PizzaState;

public class Clam extends Pizza {

    public Clam(String pizzaType, String dough, String sauce, double cost, PizzaState pizzaState) {
        super(pizzaType, dough, sauce, cost, pizzaState);
    }
}

package com.epam.homeworks.pizza;

import com.epam.homeworks.scrum.state.PizzaState;

public class Cheese extends Pizza {

    public Cheese(String pizzaType, String dough, String sauce, double cost, PizzaState pizzaState) {
        super(pizzaType, dough, sauce, cost, pizzaState);
    }
}

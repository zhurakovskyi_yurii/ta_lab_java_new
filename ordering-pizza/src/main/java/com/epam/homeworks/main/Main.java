package com.epam.homeworks.main;

import com.epam.homeworks.bakeries.Bakery;
import com.epam.homeworks.factory.PizzaFactory;
import com.epam.homeworks.pizza.Pizza;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        PizzaFactory pizzaFactory = new PizzaFactory();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            Bakery bakery = chooseCity(bufferedReader);
            Pizza pizza = pizzaFactory.bakePizza(Bakery.KYIV, bufferedReader);
            if (pizza != null) {
                logger.info(pizza + "\n");
                pizza.update();
                pizza.update();
                pizza.update();
            } else logger.info("Замовлення скасовано\n");
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Bakery chooseCity(BufferedReader bufferedReader) throws IOException {
        int choice = 0;
        Bakery bakery = Bakery.KYIV;
        while (choice != 4) {
            logger.info("Виберіть своє місто :\n");
            logger.info("1 для Львова\n");
            logger.info("2 для Києва\n");
            logger.info("3 для Дніпра\n");
            logger.info("4 для виходу\n");
            try {
                choice = Integer.parseInt(bufferedReader.readLine());
            } catch (NumberFormatException e) {
                logger.info("Невірний ввід\n");
            }
            switch (choice) {
                case 1:
                    bakery = Bakery.LVIV;
                    break;
                case 2:
                    bakery = Bakery.KYIV;
                    break;
                case 3:
                    bakery = Bakery.DNIPRO;
                    break;
                case 4:
                    break;
                default:
                    logger.info("Невірний вибір, спробуйте ще раз\n");
                    break;
            }
            if (choice <= 4 && choice >= 1)
                break;

        }
        return bakery;
    }
}

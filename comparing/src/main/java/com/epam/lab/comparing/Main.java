package com.epam.lab.comparing;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    private static Comparator<KeyValue> comparator = Comparator.comparing(KeyValue::getValue);
    public static void main(String[] args) {
        KeyValue[] keyValuesArray = fillKeyValueArray();
        logger.info("Unsorted Array of KeyValues:\n");
        printKeyValueArray(keyValuesArray);
        Arrays.sort(keyValuesArray, KeyValue::compareTo);
        logger.info("\nSorted Array of KeyValues using Comparable:\n");
        printKeyValueArray(keyValuesArray);
        Arrays.sort(keyValuesArray, comparator);
        logger.info("\nSorted Array of KeyValues using Comparator:\n");
        printKeyValueArray(keyValuesArray);
        List<KeyValue> keyValueList = fillKeyValueList();
        logger.info("\nUnsorted ArrayList of KeyValues:\n");
        printKeyValueList(keyValueList);
        keyValueList.sort(KeyValue::compareTo);
        logger.info("\nSorted ArrayList of KeyValues using Comparable:\n");
        printKeyValueList(keyValueList);
        keyValueList.sort(comparator);
        logger.info("\nSorted ArrayList of KeyValues using Comparator:\n");
        printKeyValueList(keyValueList);
        String key = keyValueList.get(0).getKey();
       // binarySearchByComparable(key, keyValueList); // ?
       // binarySearchByComparator(key, keyValueList); // ?
    }

    public static KeyValue[] fillKeyValueArray(){
        KeyValue[] keyValuesArray = new KeyValue[10];
        for (int i = 0; i < keyValuesArray.length; i++) {
            keyValuesArray[i] = KeyValueGenerator.generate();
        }
        return keyValuesArray;
    }

    public static List<KeyValue> fillKeyValueList(){
        List<KeyValue> keyValueList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            keyValueList.add(KeyValueGenerator.generate());
        }
        return keyValueList;
    }

    public static void printKeyValueArray(KeyValue[] keyValuesArray){
        for (KeyValue kv: keyValuesArray) {
            logger.info(kv + "\n");
        }
    }

    public static void printKeyValueList(List<KeyValue> keyValueList){
        for (KeyValue kv: keyValueList) {
            logger.info(kv + "\n");
        }
    }

    public static void binarySearchByComparable(String key, List<KeyValue> keyValueList){
        logger.info("\nBinary Search By Comparable\n");
        logger.info(keyValueList.get(Collections.binarySearch(keyValueList, new KeyValue(key, ""))).getValue());
    }

    public static void binarySearchByComparator(String key, List<KeyValue> keyValueList){
        logger.info("\nBinary Search By Comparator\n");
        logger.info(keyValueList.get(Collections.binarySearch(keyValueList, new KeyValue(key, ""), comparator)).getValue());
    }

}

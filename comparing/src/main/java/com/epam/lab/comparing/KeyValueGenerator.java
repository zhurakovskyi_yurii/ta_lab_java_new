package com.epam.lab.comparing;

import java.util.Random;

public class KeyValueGenerator {
    private static final Random random = new Random();
    public static KeyValue generate(){
        String key = random.ints(97, 122)
                .limit(5)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        String value = random.ints(97, 122)
                .limit(10)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
        return new KeyValue(key, value);
    }
}

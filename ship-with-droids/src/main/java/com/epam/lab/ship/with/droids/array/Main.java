package com.epam.lab.ship.with.droids.array;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

public class Main {
    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        Droid droid = new Droid("IG_88");
        File file = new File("droid.dr");
        try {
            logger.info(droid + "\n");
            serialize("droid.dr", droid);
            Droid droid2 = (Droid)deserialize(file);
            logger.info(droid2);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void serialize(String fileName, Serializable object) throws IOException {
        FileOutputStream fileOut = new FileOutputStream(fileName);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOut);
        objectOutputStream.writeObject(object);
    }

    public static void serialize(String fileName, Ship<Droid> droidShip){

    }

    public static Serializable deserialize(File fileName) throws IOException, ClassNotFoundException {
        FileInputStream fileInput = new FileInputStream(fileName);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInput);
        return (Serializable) objectInputStream.readObject();
    }
}

package com.epam.lab.ship.with.droids.array;

import java.util.Arrays;

public class Ship<T extends Droid> {
    private T[] droids;

    public Ship() {
        droids = (T[]) new Droid[1];
    }

   public void put(T droid){
        if(droids[droids.length - 1] != null) {
            droids = Arrays.copyOf(droids, droids.length + 1);
        }
       droids[droids.length - 1] = droid;
   }
    public T get(int index){
        if(index < 0 || index > droids.length -1 || droids == null)
            new IllegalArgumentException();
        return droids[index];
    }

    public int size(){
        return droids.length;
    }

    public void show(){
        for (Droid droid: droids) {
            System.out.println(droid);
        }
    }
}
